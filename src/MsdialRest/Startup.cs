﻿using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MsdialRest.Model;
using MsdialRest.Api;
using MsdialRest.Services;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.PlatformAbstractions;
using FServerClient.Api;
using FServerClient.Model;
using System;
using MsdialRest.Utility.Unzip;

namespace MsdialRest {
    ///
    public class Startup {
        ///
        public IConfigurationRoot Configuration { get; }

        /// Startup
        public Startup(IHostingEnvironment env) {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        /// This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services) {
            // Add framework services.
            services.AddOptions();

            services.Configure<Config>(Configuration);

            services.AddRouting();
            services.AddMvc().AddJsonOptions(jsonOptions => {
                jsonOptions.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
            });

            services.Configure<FormOptions>(x => {
                x.BufferBody = false;
                x.ValueLengthLimit = int.MaxValue;
                x.MultipartBodyLengthLimit = int.MaxValue; // In case of multipart
            });

            // adding custom Authorization Service
            services.AddSingleton<IFilePrepService, FilePrepService>();
            services.AddSingleton<IUnzipper, Unzipper>();
            services.AddSingleton<IFServerClient, FServerCli>((ctx) => {
                var config = Configuration.GetSection("Fserv");
                var urlstr = config.GetValue<string>("FservHost") + ":" + config.GetValue<string>("FservPort");
                Uri uri = new Uri(urlstr);
                return new FServerCli(uri);
            });

            // Add Swagger services
            services.AddSwaggerGen(c => {
                c.SwaggerDoc("v1", new Info {
                    Version = "v1",
                    Title = "MSDialRest API",
                    Description = "A simple example ASP.NET Core Web API",
                    TermsOfService = "None",
                    Contact = new Contact { Name = "Diego Pedrosa", Email = "dpedrosa@ucdavis.edu", Url = "" },
                    License = new License { Name = "Use under CC by 4.0", Url = "" }
                });

                // Set the comments path for the Swagger JSON and UI.
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "MsdialRest.xml");
                c.IncludeXmlComments(xmlPath);
            });
        }

        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory) {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            if (env.IsDevelopment() || env.IsEnvironment("Testing")) {
                loggerFactory.AddDebug(LogLevel.Debug);
                app.UseDeveloperExceptionPage();
            } else {
                loggerFactory.AddDebug(LogLevel.Information);
            }
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "MSDialRest API");
            });

            if (env.WebRootPath == null || env.WebRootPath.Equals("")) {
                env.WebRootPath = ConfigurationPath.Combine(Directory.GetCurrentDirectory(), "wwwroot");
                Directory.CreateDirectory(env.WebRootPath);
            }

            app.UseStatusCodePages();
            //app.UseStatusCodePagesWithReExecute("/Conversion/Errors/{0}");
            app.UseMvc(routes => {
                routes.MapRoute("Upload", "rest/{controller=Upload}/{action=Index}");
                routes.MapRoute("Conversion", "rest/{controller=Conversion}/{action=convert}");
                routes.MapRoute("Default", "rest/deconvolution/{controller}/{action}");
            });
        }
    }
}
