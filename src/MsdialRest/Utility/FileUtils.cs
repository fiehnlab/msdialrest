﻿using System.IO;

namespace MsdialRest.Utility {
	///
	public class FileUtils {
        ///
		public static bool IsFileLocked(FileInfo file) {
			FileStream stream = null;

			try {
				stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
			} catch (IOException) {
				//the file is unavailable because it is:
				//still being written to
				//or being processed by another thread
				//or does not exist (has already been processed)
				return true;
			} finally {
				if (stream != null)
					stream.Dispose();
			}

			//file is not locked
			return false;
		}
	}
}
