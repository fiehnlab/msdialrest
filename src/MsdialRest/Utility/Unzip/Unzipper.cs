﻿using MsdialRest.Api;
using System;
using System.IO;
using System.IO.Compression;
using System.Linq;

namespace MsdialRest.Utility.Unzip {
    /// <summary>
    /// Basic Implementation of the IUnzipper interface
    /// </summary>
    public class Unzipper : IUnzipper {
        /// <summary>
        /// Unzips a file to the output folder
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="outPath"></param>
        /// <returns></returns>
        public string Unzip(string filename, string outPath) {
            var mainfile = "";
			if (Directory.Exists(outPath)) {
				Directory.EnumerateFileSystemEntries(outPath).ToList().ForEach(entry => DeleteEntry(entry));
			}

			if (File.Exists(filename)) {
                using(ZipArchive archive = ZipFile.OpenRead(filename)) {
                    archive.ExtractToDirectory(outPath);

                    mainfile = Directory.EnumerateFileSystemEntries(outPath).SingleOrDefault(it => Formats.AcceptedRawFormats.Contains(it.Substring(it.LastIndexOf("."))));
                }
			} else {
                throw (new Exception($"Invalid zipfile '{filename}'"));
            }

            return mainfile;
        }

		private void DeleteEntry(string entry) {
			Console.WriteLine($"Deleting {entry}");
			if (File.GetAttributes(entry).HasFlag(FileAttributes.Directory)) {
				Directory.Delete(entry, true);
			} else {
				if (!entry.EndsWith(".zip")) {
					File.Delete(entry);
				}
			}
		}
    }
}
