﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MsdialRest.Utility {
    ///
    public class Formats {
        ///
        public static List<string> AcceptedFormats = new List<string>{".abf", ".cdf"};
        ///
        public static List<string> AcceptedRawFormats = new List<string> { ".d", ".raw", ".wiff" };
        ///
        public static List<string> AcceptedZippedFormats = new List<string> { ".d.zip", ".zip" };
        ///
        public static Boolean IsRawFile(string filename) {
            return AcceptedRawFormats.Contains(new FileInfo(filename).Extension) || AcceptedZippedFormats.Contains(new FileInfo(filename).Extension);
        }
        ///
        public static Boolean IsRawFile(FileInfo file) {
            return AcceptedRawFormats.Contains(file.Extension) || AcceptedZippedFormats.Contains(file.Extension);
        }
    }
}
