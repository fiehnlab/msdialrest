﻿using System;

namespace MsdialRest.Utility {
	internal class FileAlreadyScheduledException : Exception {
		public FileAlreadyScheduledException() : base("This file has been scheduled. Please rename the file and try again") {}

		public FileAlreadyScheduledException(string message) : base(message) {}

		public FileAlreadyScheduledException(string message, Exception innerException) : base(message, innerException) {}
	}
}