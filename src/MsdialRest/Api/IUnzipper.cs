﻿namespace MsdialRest.Api {
    /// <summary>
    /// 
    /// </summary>
    public interface IUnzipper {
        /// <summary>
        /// Unzips a file to the output folder
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="outPath"></param>
        /// <returns></returns>
        string Unzip(string filename, string outPath);
    }
}
