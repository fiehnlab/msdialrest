﻿using System.Collections.Generic;
using MsdialRest.Model;

namespace MsdialRest.Api {
    ///
    public interface ISampleRepository {
        ///
        Sample Get(string id);
        ///
        List<Sample> GetAll();
        ///
        List<Sample> Filter(string jsonQuery);
        ///
        Sample Insert(Sample item);
        ///
        Sample UpdatePost(string id, Sample sample);
        ///
        long DeleteAll();
        ///
        long Delete(Sample sample);
        ///
        Sample GetFirst();

    }
}
