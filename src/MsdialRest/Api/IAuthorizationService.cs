﻿using Microsoft.AspNetCore.Http;

namespace MsdialRest.Api {
    ///
    public interface IAuthorizationService {
        ///
        bool IsAllowed(HttpRequest Request, string folder);
    }
}
