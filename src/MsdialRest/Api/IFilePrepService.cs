﻿using System.IO;

namespace MsdialRest.Api {
    ///
    public interface IFilePrepService {
        ///
        FileInfo PrepFile(FileInfo file);
    }
}
