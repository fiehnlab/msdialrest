﻿namespace MsdialRest.Model {
    ///
    public class MongoConnection {
        ///
        public string ConnectionString { get; set; }
        ///
        public string Database { get; set; }
        ///
        public override string ToString() => $"{ConnectionString}/{Database}";
    }
}
