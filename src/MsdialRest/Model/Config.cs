﻿namespace MsdialRest.Model {
    ///
    public class Config {
        ///
        public MsdialConfig Msdial { get; set; }
        ///
        public AbfConverterConfig ABFConverter { get; set; }
        ///
        public FservConfig Fserv { get; set; }
        ///
        public string StorageFolder { get; set; }
    }
}
