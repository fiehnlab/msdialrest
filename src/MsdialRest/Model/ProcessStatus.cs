﻿namespace MsdialRest.Model {
    ///
    public enum ProcessStatus {
        ///
        UPLOADED, 
        ///
        PROCESSING, 
        ///
        CONVERTING,
        ///
        DECONVOLUTED, 
        ///
        ERROR, 
        ///
        UNKNOWN
    }
}
