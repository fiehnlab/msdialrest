﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace MsdialRest.Model {
    ///
    public class Sample {

        ///
        public string Id { get; }
        ///
        public string Filename { get; set; }
        ///
        public ProcessStatus Status { get; set; } = ProcessStatus.UNKNOWN;
        ///
        public string Format { get; set; } = string.Empty;
        ///
        public List<string> Errors { get; set; } = new List<string>();

        /// <summary>
        /// Constructor. Sets the Id of the new sample based on the name (SHA256)
        /// </summary>
        /// <param name="name"></param>
        public Sample(string name) {
            using (SHA256 hash = SHA256.Create()) {
                Id = string.Concat(hash
                  .ComputeHash(Encoding.UTF8.GetBytes(name))
                  .Select(item => item.ToString("x2")))
                  .Substring(0, 10);
            }

            Filename = name;
        }

        /// <summary>
        /// Equals override
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj) {
            if(!(obj is Sample)) { return false; }
            Sample other = obj as Sample;

            return Id.Equals(other.Id) &&
                Filename.Equals(other.Filename) &&
                Status.Equals(other.Status) &&
                Format.Equals(other.Format);
        }

        /// <summary>
        /// HashCode override
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode() {
            return Id.GetHashCode();
        }
    }
}
