﻿namespace MsdialRest.Model.Response {
    ///
    public class ScheduleResponse {
        ///
        public string filename { get; set; }
        ///
        public string link { get; set; }
        ///
        public string message { get; set; } = "";
        ///
        public string error { get; set; } = "";
    }
}
