﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MsdialRest.Model.Response {
    /// <summary>
    /// Reponse from Process endpoint
    /// </summary>
    public class ProcessResponse {
        /// <summary>
        /// 
        /// </summary>
        public string filename { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string link { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string message { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string error { get; set; }

        /// <summary>
        /// ToString override
        /// </summary>
        /// <returns></returns>
        public override string ToString() {
            return $"filename: {filename}; link: {link}; message: {message}; error: {error}";
        } 
    }
}
