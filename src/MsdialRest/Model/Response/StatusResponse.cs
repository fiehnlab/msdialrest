﻿namespace MsdialRest.Model.Response {
    ///
    public class StatusResponse {
        ///
        public string filename { get; set; }
        ///
        public string link { get; set; }
        ///
        public ProcessStatus status;
        ///
        public string message { get; set; } = "";
        ///
        public string error { get; set; } = "";
    }
}
