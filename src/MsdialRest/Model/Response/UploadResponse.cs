﻿namespace MsdialRest.Model.Response {
    ///
    public class UploadResponse {
        ///
        public string filename { get; set; }
        ///
        public string link { get; set; }
        ///
        public string message { get; set; } = null;
        ///
        public string stackTrace { get; set; } = null;
        ///
        public string fileToProcess { get; set; } = null;
    }
}
