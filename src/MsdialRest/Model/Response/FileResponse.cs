namespace MsdialRest.Model.Response {
    ///
    public class FileResponse {
        ///
        public string filename { get; set; } = "";
        ///
        public bool exists { get; set; } = false;

        ///
        public override string ToString() {
            return $"filename: {filename}; exists: {exists}";
        } 
    }
}