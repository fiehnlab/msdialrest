﻿namespace MsdialRest.Model {
    ///
    public class FservConfig {
        ///
        public string FservHost { get; set; }
        ///
        public string FservPort { get; set; }
        ///
        public string RestBase { get => "rest/file"; }
    }
}
