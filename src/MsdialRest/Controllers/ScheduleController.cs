﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Microsoft.Extensions.Options;
using MsdialRest.Model;
using MsdialRest.Model.Response;
using MsdialRunner;
using MsdialRunner.Exceptions;
using MsdialRest.Api;
using MsdialRest.Utility;
using MsdialRunner.Model;

namespace MsdialRest.Controllers {
    ///
    [Route("rest/deconvolution/[controller]")]
	public class ScheduleController : Controller {
		private readonly IHostingEnvironment env;
		private readonly ILogger<ScheduleController> logger;
        private readonly ILoggerFactory logFac;
        private readonly Config config;

        private readonly string DEFAULT_PARAMS_FILEPATH;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="environment"></param>
        /// <param name="_logFac"></param>
        /// <param name="_config"></param>
        public ScheduleController(IHostingEnvironment environment, ILoggerFactory _logFac, IOptions<Config> _config) {
			env = environment;
            logFac = _logFac;
			logger = logFac.CreateLogger<ScheduleController>();
            config = _config.Value;

			DEFAULT_PARAMS_FILEPATH = Path.Combine(env.ContentRootPath, "resources", "default-lcparams.txt");
		}

        /// <summary>
        /// Calls MSDialRunner to start processing the given id, with <b>default</b> parameters
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        [HttpGet("{filename}", Name = "ScheduleFile")]
		public IActionResult Get(string filename) {

            logger.LogInformation($"Scheduling file in {filename} for deconvolution");
            return RunMsdial(filename, DEFAULT_PARAMS_FILEPATH);
		}

        // POST rest/deconvolution/schedule/{guid}     with json body containing parameters described in schema
        /// <summary>
        /// Calls MSDialRunner to start processing the given id, with <b>custom</b> parameters
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="jsonParams"></param>
        /// <returns></returns>
        [HttpPost("{filename}")]
		public IActionResult Post(string filename, [FromBody] JObject jsonParams) {

            if (jsonParams == null) { return BadRequest(new ScheduleResponse() { error = $"Need custom parameters in json format. Please check schema: {env.WebRootPath}/Schemas/msdial-lcschema.json" }); }

			//load schema and validate jsonParams
			JSchema schema = JSchema.Parse(System.IO.File.ReadAllText($@"{env.WebRootPath}/Schemas/msdial-schema.json"));

            //TODO: write test for posting parameters

            if (!jsonParams.IsValid(schema, out IList<string> paramErrors)) {
                foreach (var error in paramErrors) {
                    logger.LogError(error);
                }
                return BadRequest(new ScheduleResponse() { error = $"Invalid custom paramters:\n{string.Join("\n", paramErrors)}\n Please check schema: {env.WebRootPath}/Schemas/msdial-lcschema.json" });
            }
            var parameters = jsonParams.ToObject<GCParameters>();

			if (parameters.Identification == null) {
				parameters.Identification = new Identification() {
					RetentionType= "RT",
					RICompound="Fames",
					RetentionTimeTolerance= 0.5,
					RetentionIndexTolerance= 3000,
					EISimilarityLibraryTolerance= 70,
					IdentificationScoreCutOff= 70
				};
			} else {
                parameters.Identification.RetentionType = "RT";
                parameters.Identification.RICompound = "Fames";
            }

            if (parameters.Alignment == null) {
				parameters.Alignment = new Alignment() {
					RetentionTimeTolerance= 0.075,
					EISimilarityTolerance= 70,
					RetentionTimeFactor= 0.5,
					EISimilarityFactor= 0.5,
					PeakCountFilter= 0,
					QCAtLeastFilter= true
				};
			}

			// create file with params
			var paramPath = Path.Combine(env.WebRootPath, filename, "custom.params");
			System.IO.File.WriteAllText(paramPath, parameters.ToString());

            // call msdial runner
            try {
				return RunMsdial(filename, paramPath);
			} catch (FileAlreadyScheduledException ex) {
				return BadRequest(new ScheduleResponse() { message = "File was already scheduled\n" + ex.Message });
			}
		}


        /// <summary>
        /// Calls the msdial runner service
        /// </summary>
        /// <param name="workFolder"></param>
        /// <param name="paramsFilePath"></param>
        /// <returns></returns>
		private IActionResult RunMsdial(string workFolder, string paramsFilePath) {

            logger.LogInformation($"\nPARAMS: workFolder={workFolder}\t paramsFilePath={paramsFilePath}");

			var abfFilePath = GetFileToProcess(Path.Combine(config.StorageFolder, workFolder));

            if (string.IsNullOrEmpty(abfFilePath)) {
                var msg = $"Can't find a file to process in folder '{workFolder}'";
                logger.LogError(msg);
                throw new FileNotFoundException(msg);
			}

			var fi = new FileInfo(abfFilePath);
            var resultFolder = fi.DirectoryName;

			// call msdial runner with default params
			try {
                /* runner path = Absolute path to MsdialConsoleApp.exe
                 * method = one of gcms, lcmsdda or lcmsdia
                 * rawFilePath = Absolute path to directory of file to be processed
                 * resultFolder = Absolute path to directory to save the deconvoluted data
                 * paramsFilePath = Absolute path to the parameters file
                 * logger = well... a Microsoft.Extensions.Logging.ILogger logger injected into the controller...
                */
                var method = "lcmsdda";

                var response = MsdialRun.Run(config.Msdial.RunnerPath, method, fi.DirectoryName, resultFolder, paramsFilePath, logFac);

                var resultUri = string.Concat(
                            Request.Scheme,
                            "://",
                            Request.Host.ToUriComponent(),
                            Request.PathBase.ToUriComponent(),
                            $"/rest/deconvolution/status/{workFolder}");

                logger.LogInformation("Deconvolution successful.");
                return Ok(new ScheduleResponse() { filename = fi.Name, message = $"{fi.Name} scheduled with parameters from {new FileInfo(paramsFilePath).Name}", link = resultUri });

            } catch (MsdialProcessException ex) {
                logger.LogError(ex.Message);
                return BadRequest(new ScheduleResponse() { error = ex.Message + "\t\n" + ex.MsdialError });
			}
		}

        private string GetFileToProcess(string folder) {
            var convPath = Path.Combine(folder);

            var mainfile = Directory.EnumerateFileSystemEntries(convPath)
                    .Where(file => Formats.AcceptedFormats.Contains(file.Substring(file.LastIndexOf(".")))).First();

            return mainfile;
        }
	}
}
