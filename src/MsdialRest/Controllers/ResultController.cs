﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MsdialRest.Api;
using MsdialRest.Model;
using System.IO;
using System.Linq;

namespace MsdialRest.Controllers {
    ///
    [Route("rest/deconvolution/[controller]")]
    public class ResultController : Controller {
		private readonly IHostingEnvironment env;
		private readonly ILogger<ResultController> logger;
        private readonly Config config;

		private static readonly string CONTENT_TYPE = "application/octet-stream";

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="environment"></param>
        /// <param name="log"></param>
        /// <param name="_config"></param>
        public ResultController(IHostingEnvironment environment, ILogger<ResultController> log, IOptions<Config> _config) {
			env = environment;
			logger = log;
            config = _config.Value;
		}

        /// <summary>
        /// Downloads a result file for the given process id
        /// </summary>
        /// <param name="filename">process id containing the results to be downloaded</param>
        /// <returns>A byte array holding the results data or a 400 error in case of missing result file</returns>
        [HttpGet("{filename}")]
		public IActionResult Download(string filename) {

            var resultFile = GetResultFile(filename);

            if (System.IO.File.Exists(resultFile)) {
                try {
                    FileContentResult result = new FileContentResult(System.IO.File.ReadAllBytes(resultFile), CONTENT_TYPE) {
                        FileDownloadName = filename
                    };
                    HttpContext.Response.ContentType = CONTENT_TYPE;

                    logger.LogInformation($"Downloading results ({result.FileContents.Length} bytes)");
                    return result;
                } catch (IOException ex) {
                    logger.LogInformation($"Downloading failed ({ex.Message})");
                    return BadRequest(new { error = "Can't open file for download. " + ex.Message });
                }

            } else {
				var msg = $"Can't find a result file in {filename}/result";
                logger.LogInformation($"Downloading failed ({msg})");
                return BadRequest(new { error = msg });
			}
		}

        /// <summary>
        /// Finds the results file in the proper folder
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private string GetResultFile(string filename) {
            var dir = Path.Combine(config.StorageFolder, filename);

            return Directory.GetFiles(dir).Where(it => it.EndsWith(".msdial")).Single();
        }
    }
}
