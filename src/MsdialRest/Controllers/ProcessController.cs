﻿using FServerClient.Api;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MsdialRest.Api;
using MsdialRest.Model;
using MsdialRest.Model.Response;
using MsdialRunner;
using MsdialRunner.Exceptions;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ABFConverterRunner;
using MsdialRest.Utility;

namespace MsdialRest.Controllers {
    /// <summary>
    /// Processes deconvolution requests from Carrot.
    /// </summary>
    [Route("rest/deconvolution/[controller]")]
    public class ProcessController : Controller {
        private readonly ILogger<ProcessController> logger;
        private readonly ILoggerFactory logFac;
        private readonly Config config;
        private readonly IFServerClient fsClient;
        private readonly IFilePrepService prepSvc;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_logFac"></param>
        /// <param name="_config"></param>
        /// <param name="fsCli"></param>
        /// <param name="_prepSvc"></param>
        public ProcessController(ILoggerFactory _logFac, IOptions<Config> _config, IFServerClient fsCli, IFilePrepService _prepSvc) {
            logFac = _logFac;
            logger = logFac.CreateLogger<ProcessController>();
            config = _config.Value;
            fsClient = fsCli;
            prepSvc = _prepSvc;
        }

        /// <summary>
        /// Calls the deconvolution service for a file
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        [HttpGet("{filename}")]
        public IActionResult Process(string filename) {
            ProcessResponse response;

            if (string.IsNullOrWhiteSpace(filename)) {
                return BadRequest(new { filename = filename, error = "invalid filename" });
            } else if (!filename.EndsWith(".abf")) {
                return BadRequest(new { filename = filename, error = "invalid file type, please use .abf" });
            }

            var store = Path.Combine(config.StorageFolder, filename);
            var file = new FileInfo(Path.Combine(store, filename));

            logger.LogInformation($"File to process: {file.FullName}");

            if (!file.Exists) {
                return BadRequest(new ProcessResponse() { filename = filename, error = $"{filename} not found, please upload" });
            }

            var process = "";
            try {
                process = RunMsdial(file);
                logger.LogInformation($"File {file.Name} processed.");
                file.Delete();
            } catch(Exception ex) {
                logger.LogError(ex.Message);
                return BadRequest(new ProcessResponse() { filename = filename, error = ex.Message });
            }

            var link = $"{fsClient.GetAddress()}/rest/file/download/{process}";
            response = new ProcessResponse() { filename = process, link = link, message = $"{filename} processed succesfully." };
            return Ok(response);
        }

        private string RunMsdial(FileInfo file, string method = "lcmsdda") {
            var DEFAULT_PARAMS_FILEPATH = Path.Combine(Directory.GetCurrentDirectory(), "Resources", "default-lcparams.txt");

            var resultFolder = file.DirectoryName;

            // call msdial runner with default params (creates *.msdial file)
            try {
                /* runner path = Absolute path to MsdialConsoleApp.exe
                 * method = one of gcms, lcmsdda or lcmsdia
                 * rawFilePath = Absolute path to directory of file to be processed
                 * resultFolder = Absolute path to directory to save the deconvoluted data
                 * paramsFilePath = Absolute path to the parameters file
                 * logger = well... a Microsoft.Extensions.Logging.ILogger logger injected into the controller...
                */
                logger.LogInformation($"Processing {file.Name}...");
                var response = MsdialRun.Run(config.Msdial.RunnerPath, method, file.FullName, resultFolder, DEFAULT_PARAMS_FILEPATH, logFac);
                var filenameNoExt = file.Name.Replace(file.Extension, "");

                // cleanup
                var list = Directory.GetFiles(config.StorageFolder, "*.*", SearchOption.AllDirectories)
                    .Where(f => Regex.IsMatch(f, $"{filenameNoExt}_\\d+\\.(?:dcl|pai)")).ToList();
                list.ForEach(f => new FileInfo(f).Delete());
                file.Delete();

                return $"{filenameNoExt}.msdial";

            } catch (MsdialProcessException ex) {
                logger.LogError(ex.Message);
                return $"Could not process file {file}. Error:\"{ex.Message}\"";
            }
        }

    }
}
