using FServerClient.Api;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MsdialRest.Api;
using MsdialRest.Model;
using MsdialRest.Utility;
using MsdialRest.Model.Response;
using MsdialRunner;
using MsdialRunner.Exceptions;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;
using Microsoft.AspNetCore.Http;
using MsdialRest.Utility.Unzip;

namespace MsdialRest.Controllers {
    /// <summary>
    /// Processes deconvolution requests from Carrot.
    /// </summary>
    [Route("rest/[controller]")]
    public class FileController : Controller {
        private readonly ILogger<FileController> logger;
        private readonly ILoggerFactory logFac;
        private readonly Config config;
        private readonly IFServerClient fsClient;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_logFac"></param>
        /// <param name="_config"></param>
        /// <param name="fsCli"></param>
        public FileController(ILoggerFactory _logFac, IOptions<Config> _config, IFServerClient fsCli) {
            logFac = _logFac;
            logger = logFac.CreateLogger<FileController>();
            config = _config.Value;
            fsClient = fsCli;
        }

        /// <summary>
        /// Checks if a file exists on the server
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        [HttpGet("[action]/{filename}")]
        public IActionResult Exists(string filename) {
            FileResponse response = new FileResponse() { filename = filename };

            try {
                var storage = new DirectoryInfo(config.StorageFolder);
                if(Directory.GetFiles(Path.Combine(config.StorageFolder,filename), filename).Length == 1) {
                    response.exists = true;
                }
            } catch(DirectoryNotFoundException) {
                response.exists = false;
            }

            return Ok(response);
        }

        /// <summary>
        /// Creates a List of files on the server
        /// </summary>
        /// <returns></returns>
        [HttpGet("[action]")]
        public IActionResult List() {
            var files = new List<string>();
            var storage = new DirectoryInfo(config.StorageFolder);
            files.AddRange(Directory.GetFiles(config.StorageFolder, "*", SearchOption.TopDirectoryOnly));

            Console.WriteLine($"files: ({files.Count}): {string.Join("; ", files)}");

            return Ok(string.Join("; ", files));
        }

        /// <summary>
        /// Uploads a file to be deconvoluted
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
		[HttpPost("[action]")]
		[Consumes("multipart/form-data")]
		public async Task<IActionResult> Upload(IFormFile file) {
			var fileInfo = new FileInfo(Path.Combine(config.StorageFolder, file.FileName));
			var sample = new Sample(fileInfo.Name) { Format = fileInfo.Extension.ToLower()};
            var workFolder = new DirectoryInfo(Path.Combine(config.StorageFolder, sample.Filename));
            workFolder.Create();

            try {
                if (file != null && file.Length > 0) {
					var save = new FileInfo(Path.Combine(workFolder.FullName, sample.Filename));

					using (FileStream fs = new FileStream(save.FullName, FileMode.Create)) {
						await file.CopyToAsync(fs);
						fs.Flush();
					}

                    var scheduleUri = "";
                    if (Formats.AcceptedFormats.Contains(sample.Format)) {  // unzipped file ready for processing
                        scheduleUri = string.Concat(
                        Request.Scheme,
                        "://",
                        Request.Host.ToUriComponent(),
                        Request.PathBase.ToUriComponent(),
                        $"/rest/deconvolution/process/{sample.Filename}");

                        logger.LogInformation("Upload successfull.");
                        return Ok(new UploadResponse() { filename = save.Name, link = scheduleUri, fileToProcess = save.Name });
                    } else {                                                // from an originally zipped file

                        var zip = sample.Filename;
                        var mainFile = new Unzipper().Unzip(save.FullName, workFolder.FullName);
                        //remove zipfile after successfull unzipping
                        new FileInfo(Path.Combine(workFolder.FullName, zip)).Delete();

                        scheduleUri = string.Concat(
                        Request.Scheme,
                        "://",
                        Request.Host.ToUriComponent(),
                        Request.PathBase.ToUriComponent(),
                        $"/rest/conversion/convert/{sample.Filename}");

                        logger.LogInformation("Upload successfull.");
                        return Ok(new UploadResponse() { filename = save.Name, link = scheduleUri, fileToProcess = mainFile });
                    }

                } else {
                    var msg = "Upload Error, no data found in request body.";
                    logger.LogError(msg);
                    workFolder.Delete(true);
                    return BadRequest(new UploadResponse() { message = msg, filename = sample.Filename, link = "" });
				}
			} catch(Exception ex) {
                logger.LogError($"Upload Error: {ex.Message}");
                workFolder.Delete(true);
                return BadRequest(new UploadResponse() { message = ex.Message, stackTrace = ex.StackTrace, filename = file.FileName });
			}
		}
    }
}