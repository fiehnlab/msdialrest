﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MsdialRest.Api;
using MsdialRest.Model;
using System;
using System.IO;

namespace MsdialRest.Controllers {
    ///
    [Route("rest/deconvolution")]
    public class DeleteController : Controller {
        private readonly IHostingEnvironment env;
        private readonly ILogger<DeleteController> logger;
        private readonly Config config;

        ///
        public DeleteController(IHostingEnvironment _env, ILogger<DeleteController> _logger, IOptions<Config> conf) {
            env = _env;
            logger = _logger;
            config = conf.Value;
        }

        /// <summary>
        /// Cleanup action, will delete all content in processing folder
        /// </summary>
        /// <param name="filename">GUID representing the folder to be cleaned up</param>
        /// <returns>200 if deletion was successful, 401 if the request is unaouthorized, 404 if the GUID doesn't exist</returns>
        [HttpDelete("[action]/{filename}")]
        public IActionResult cleanup(string filename) {
            var requestedDir = Path.Combine(config.StorageFolder, filename);

            try {
                logger.LogInformation($"About to delete {filename}");
                Directory.Delete(requestedDir, true);
                return Ok();
            } catch(Exception ex) {
                logger.LogError(ex.Message);
                return NotFound();
            }
        }
    }
}
