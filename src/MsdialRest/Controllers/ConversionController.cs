﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MsdialRest.Api;
using MsdialRest.Model;
using MsdialRest.Model.Response;
using MsdialRest.Utility;
using ABFConverterRunner;
using ABFConverterRunner.Exceptions;

namespace MsdialRest.Controllers {
    ///
    [Route("rest/[controller]")]
    public class ConversionController : Controller {
        private readonly IHostingEnvironment env;
        private readonly ILogger<ConversionController> logger;
        private readonly ILoggerFactory loggerFactory;
        private readonly Config config;

        /// <summary>
        /// Constructor, Creates a ConversionController
        /// </summary>
        /// <param name="_env">Injected dependency holding environment context</param>
        /// <param name="_loggerFactory">Injected dependency to create loggers for this class and helpers</param>
        /// <param name="_config">Injected dependency holding custom configuration</param>
        public ConversionController(IHostingEnvironment _env, ILoggerFactory _loggerFactory, IOptions<Config> _config) {
            env = _env;
            loggerFactory = _loggerFactory;
            logger = loggerFactory.CreateLogger<ConversionController>();
            config = _config.Value;
        }

        /// <summary>
        /// Action that converts a raw file in the passed in folder
        /// </summary>
        /// <param name="folder">Sample.Filename to be converted</param>
        /// <returns>A ConversionResponse with information to process the sample or 400 error in case of failure with deescription of problem</returns>
        [HttpGet("[action]/{folder}")]
        public IActionResult convert(string folder) {
            try {
                var rawFile = GetRawFile(folder);
                logger.LogInformation($"Converting file {rawFile}");

                return RunAbfConverter(folder, rawFile);
            } catch (FileNotFoundException ex) {
                logger.LogError(ex.Message);
                return BadRequest(new ConversionResponse() { filename = "", link = "", error = "There are no files to convert" });
            } catch (InvalidOperationException ex) {
                logger.LogError(ex.Message);
                return BadRequest(new ConversionResponse() { filename = "", link = "", error = $"The folder '{folder}' doen't exist." });
            }
        }

        /// <summary>
        /// ConsoleAbfConverter runner method. Converts the file passed in the parameter and saves the abf in the 'converted' folder
        /// </summary>
        /// <param name="folder">Sample's working folder (Sample.Id {GUID})</param>
        /// <param name="rawFilePath">Sample.filename</param>
        /// <returns>A ConversionResponse with information to process the sample or with 400 error in case of failure with description of problem</returns>
        protected IActionResult RunAbfConverter(string folder, string rawFilePath) {
            var fi = new FileInfo(rawFilePath);
            var convertedFile = fi.Name.Replace(fi.Extension, ".abf");
            var dest = Path.Combine(config.StorageFolder, folder);
            Directory.CreateDirectory(dest);

            // call msdial runner with default params
            try {

                var procresult = AbfConverterRunner.Run(config.ABFConverter.RunnerPath, rawFilePath, dest, loggerFactory);

            } catch (ConversionException ex) {
                logger.LogError("Conversion error: " + ex.Message);
                return BadRequest(new ConversionResponse() { filename = fi.Name, link = "", message = $"There was a problem running the converter. [{ex.ConverterCode}] {ex.ConverterOutput}", error = ex.Message + "  " + ex.ConverterError });
            }

            var resultUri = string.Concat(
                Request.Scheme, "://",
                Request.Host.ToUriComponent(),
                Request.PathBase.ToUriComponent(),
                $"/rest/deconvolution/schedule/{folder}");

            logger.LogInformation("\tConversion successful.");
            return Ok(new ConversionResponse() { filename = fi.Name, message = $"{fi.Name} conversion successfull", link = resultUri });
        }

        ///
        protected string GetRawFile(string folder) {
            var data = new DirectoryInfo(Path.Combine(config.StorageFolder, folder)).GetFileSystemInfos().Where(f => Formats.AcceptedRawFormats.Contains(f.Extension)).Single().FullName;
            if (data.EndsWith(".d.zip")) {
                data = data.Replace(".zip", "");
            }
            return data;
        }
    }
}
