﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.IO;

namespace MsdialRest.Controllers {
	///
    [Route("rest/deconvolution/[controller]")]
    public class SchemasController : Controller {
        private readonly IHostingEnvironment env;
        private readonly ILogger<SchemasController> logger;

		///
        public SchemasController(IHostingEnvironment env, ILogger<SchemasController> log) {
            this.env = env;
            this.logger = log;
        }

		///
		[HttpGet("[action]")]
		public IActionResult gcschema() {
			return Ok(System.IO.File.ReadAllText(Path.Combine(env.WebRootPath,"Schemas","msdial-gcschema.json")));
		}

		///
		[HttpGet("[action]")]
		public IActionResult lcschema() {
			return Ok(System.IO.File.ReadAllText(Path.Combine(env.WebRootPath,"Schemas","msdial-lcschema.json")));
		}
	}
}
