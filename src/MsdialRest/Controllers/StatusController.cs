﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MsdialRest.Model;
using MsdialRest.Model.Response;
using System.IO;
using System.Linq;

namespace MsdialRest.Controllers {
    ///
    [Route("rest/deconvolution/[controller]")]
    public class StatusController : Controller {
		private readonly IHostingEnvironment env;
		private readonly ILogger<StatusController> logger;
        private readonly Config config;

        ///
		public StatusController(IHostingEnvironment environment, ILogger<StatusController> log, IOptions<Config> conf) {
			env = environment;
			logger = log;
            config = conf.Value;
		}

        /// <summary>
        /// Checks the status of the given process
        /// </summary>
        /// <param name="filename">process id to be checked</param>
        /// <returns></returns>
		// GET rest/deconvolution/status/{GUID}
		[HttpGet("{filename}")]
        public IActionResult Get(string filename) {
            var resultUri = string.Concat(
					Request.Scheme,
					"://",
					Request.Host.ToUriComponent(),
					Request.PathBase.ToUriComponent(),
					$"/rest/deconvolution/result/{filename}");

            var resultFile = GetResultFile(filename);

            try {
                System.IO.File.OpenWrite(resultFile).Dispose();
				return Ok(new StatusResponse() { filename = new FileInfo(resultFile).Name, status = ProcessStatus.DECONVOLUTED, link = resultUri.ToString() });
			} catch(IOException) {
				return Ok(new StatusResponse() { filename = new FileInfo(resultFile).Name, status = ProcessStatus.PROCESSING, link = "" });
			}
		}

        private string GetResultFile(string folder) {
            var dir = Path.Combine(config.StorageFolder, folder);

            return Directory.GetFiles(dir).Where(it => it.EndsWith("msdial")).Single();
        }
    }
}
