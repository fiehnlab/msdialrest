﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Threading.Tasks;
using MsdialRest.Model;
using MsdialRest.Model.Response;
using MsdialRest.Utility;
using MsdialRest.Utility.Unzip;
using Microsoft.Extensions.Options;

namespace MsdialRest.Controllers {
    ///
    [Route("rest/[controller]")]
    public class UploadController : Controller {
        private readonly IHostingEnvironment env;
        private readonly ILogger<UploadController> logger;
        private readonly Config config;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="environment"></param>
        /// <param name="log"></param>
        /// <param name="_config"></param>
        public UploadController(IHostingEnvironment environment, ILogger<UploadController> log, IOptions<Config> _config) {
            env = environment;
            logger = log;
            config = _config.Value;
        }

        /// <summary>
        /// Shows an upload form
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Index() {
            return View("Upload");
        }

        /// <summary>
        /// Uploads a file to be deconvoluted
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
		[HttpPost]
        [Consumes("multipart/form-data")]
        public async Task<IActionResult> Upload(IFormFile file) {
            var fileInfo = new FileInfo(Path.Combine(config.StorageFolder, file.FileName));
            logger.LogInformation($"filename: {fileInfo.Name}");
            var sample = new Sample(fileInfo.Name) { Format = fileInfo.Extension.ToLower() };
            var workFolder = new DirectoryInfo(Path.Combine(config.StorageFolder, sample.Filename));
            workFolder.Create();

            var save = "";
            try {
                if (file != null && file.Length > 0) {
                    save = Path.Combine(workFolder.FullName, sample.Filename);

                    using (FileStream fs = new FileStream(save, FileMode.Create)) {
                        await file.CopyToAsync(fs);
                        fs.Flush();
                    }
                } else {
                    var msg = "Upload Error, no data found in request body.";
                    logger.LogError(msg);
                    workFolder.Delete(true);
                    return BadRequest(new UploadResponse() { message = msg, filename = sample.Filename, link = "" });
                }
            } catch (IOException ex) {
                logger.LogError($"Upload Error: {ex.Message}");
                workFolder.Delete(true);
                return BadRequest(new UploadResponse() { message = ex.Message, stackTrace = ex.StackTrace, filename = file.FileName });
            }


            try {
                // create empty file to use as authorization token
                //new FileInfo(Path.Combine(workFolder.FullName, sample.Filename)).CreateText().Dispose();
            } catch (IOException ex) {
                logger.LogError($"Error creating authorization token: {ex.Message}");
                workFolder.Delete(true);
                return BadRequest(new UploadResponse() { message = ex.Message, stackTrace = ex.StackTrace, filename = file.FileName });
            }

            try {
                // if file is zip, unzipp it
                if (sample.Format.Equals(".zip")) {
                    var zip = sample.Filename;
                    var mainFile = new Unzipper().Unzip(save, workFolder.FullName);

                    sample.Filename = new FileInfo(mainFile).Name;

                    //remove zipfile after successfull unzipping
                    new FileInfo(Path.Combine(workFolder.FullName, zip)).Delete();
                }
                    var scheduleUri = "";
                    if (Formats.AcceptedFormats.Contains(sample.Format)) {  // unzipped file ready for processing
                        scheduleUri = string.Concat(
                        Request.Scheme,
                        "://",
                        Request.Host.ToUriComponent(),
                        Request.PathBase.ToUriComponent(),
                        $"/rest/deconvolution/schedule/{sample.Id}");

                        logger.LogInformation("Upload successfull.");
                        return Ok(new UploadResponse() { filename = sample.Filename, link = scheduleUri });
                    } else {                                                // from an originally zipped file
                        scheduleUri = string.Concat(
                        Request.Scheme,
                        "://",
                        Request.Host.ToUriComponent(),
                        Request.PathBase.ToUriComponent(),
                        $"/rest/conversion/convert/{sample.Id}");

                        logger.LogInformation("Upload successfull.");
                        return Ok(new UploadResponse() { filename = sample.Filename, link = scheduleUri });
                    }

            } catch (Exception ex) {
                logger.LogError($"Upload Error: {ex.Message}");
                workFolder.Delete(true);
                return BadRequest(new UploadResponse() { message = ex.Message, stackTrace = ex.StackTrace, filename = file.FileName });
            }
        }
    }
}
