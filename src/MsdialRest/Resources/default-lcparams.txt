﻿#Data type
MS1 data type: Centroid
MS2 data type: Centroid
Ion mode: Positive
DIA file: 

#Data collection parameters
Retention time begin: 0.5
Retention time end: 12.5
Mass range begin: 100
Mass range end: 1700

#Centroid parameters
MS1 tolerance for centroid: 0.01
MS2 tolerance for centroid: 0.1

#Peak detection parameters
Smoothing method: LinearWeightedMovingAverage
Smoothing level: 1
Minimum peak width: 5
Minimum peak height: 2500
Mass slice width: 0.1

#Deconvolution parameters
Sigma window value: 0.1
Amplitude cut off: 10

#MSP file and MS/MS identification setting
MSP file:
Retention time tolerance for identification: 0.5
Accurate ms1 tolerance for identification: 0.01
Accurate ms2 tolerance for identification: 0.01
Identification score cut off: 70

#Text file and post identification (retention time and accurate mass based) setting
Text file:
Retention time tolerance for post identification: 0.1
Accurate ms1 tolerance for post identification: 0.01
Post identification score cut off: 75

#Alignment parameters setting
Retention time tolerance for alignment: 0.1
MS1 tolerance for alignment: 0.025
Retention time factor for alignment: 0.5
MS1 factor for alignment: 0.5
Peak count filter: 0
QC at least filter: True
