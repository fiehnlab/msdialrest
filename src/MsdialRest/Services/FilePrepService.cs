﻿using System.IO;
using ABFConverterRunner;
using MsdialRest.Api;
using MsdialRest.Model;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ABFConverterRunner.Exceptions;
using System;

namespace MsdialRest.Services {
    /// <summary>
    /// 
    /// </summary>
    public class FilePrepService : IFilePrepService {
        private readonly ILoggerFactory logFac;
        private readonly ILogger<FilePrepService> logger;
        private readonly Config config;
        private readonly IUnzipper unzip;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_logFac"></param>
        /// <param name="_config"></param>
        /// <param name="_unzip"></param>
        public FilePrepService(ILoggerFactory _logFac, IOptions<Config> _config, IUnzipper _unzip) {
            config = _config.Value;
            logFac = _logFac;
            logger = logFac.CreateLogger<FilePrepService>();
            unzip = _unzip;
        }


        /// <summary>
        /// Converts the input file if necessary
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public FileInfo PrepFile(FileInfo file) {
            if(file.Extension.Equals(".abf")) {
                return file;
            }

            FileInfo convertedFile;

            //unzip
            var tmp = Guid.NewGuid().ToString();
            var unzipped = unzip.Unzip(file.FullName, Path.Combine(config.StorageFolder, "raw", tmp));
            file.Delete();

            //and convert
            var msg = AbfConverterRunner.Run(config.ABFConverter.RunnerPath, unzipped, Path.Combine(config.StorageFolder, "converted"), logFac);
            if (msg.Code == 0) {
                Directory.Delete(Path.Combine(config.StorageFolder, "raw", tmp), true);
                convertedFile = new FileInfo(msg.Destination);
            } else {
                throw new ConversionException($"Can't convert file {file.FullName}.\n{msg.Error}");
            }

            return convertedFile;
        }
    }
}
