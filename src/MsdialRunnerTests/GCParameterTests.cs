﻿using MsdialRunner.Model;
using Xunit;
using Xunit.Abstractions;

namespace MsdialRunnerTests {
    public class GCParameterTests {
		private readonly string schema = "msdial-gcschema.json";
		private readonly ITestOutputHelper output;
		GCParameters _params;

		public GCParameterTests(ITestOutputHelper output) {
			this.output = output;
		}

		private static readonly string jsonStr = @"{ ""DataType"": {
    ""datatype"": ""Centroid"",
    ""ionmode"": ""Positive"",
    ""accuracy"": ""IsNominal""
  },
  ""DataCollection"": {
    ""rtbegin"": 1,
    ""rtend"": 15,
    ""mzrangebegin"": 50,
    ""mzrangeend"": 1500
  },
  ""PeakDetection"": {
    ""smoothingmethod"": ""LinearWeightedMovingAverage"",
    ""smoothinglevel"": 2,
    ""avgpeakwidth"": 20,
    ""minpeakheight"": 2000,
    ""mzslicewidth"": 0.5,
    ""mzaccuracy"": 0.5
  },
  ""Deconvolution"": {
    ""sigmawindowvalue"": 0.1,
    ""amplitudcutoff"": 10
  },
  ""Identification"": {
    ""retentiontype"": ""RT"",
    ""ricompound"": ""Fames"",
    ""rttolerance"": 0.5,
    ""ritolerance"": 3000,
    ""eisimilaritytolerance"": 70,
    ""idscorecutoff"": 70
  },
  ""Alignment"": {
	""rttolerance"": 0.075,
    ""eisimilaritytolerance"": 70,
    ""rtfactor"": 0.5,
    ""eisimilarityfactor"": 0.5,
    ""peakcountfilter"": 0,
    ""qcatleastfilter"": true
  }
}";

		[Fact]
		public void CanCreateTest() {
			var p = GCParameters.FromJson(jsonStr);

			Assert.NotNull(p);
			Assert.IsType<GCParameters>(p);
			Assert.NotEmpty(p.ToString());
		}

		[Fact]
		public void TestDTString() {
			var dt = new DataTypeParam() { DataType = "Centroid", IonMode = "Positive", AccuracyType = "IsNominal" };

			output.WriteLine(dt.ToString());
			Assert.Contains("Data type: Centroid", dt.ToString());
			Assert.Contains("Ion mode: Positive", dt.ToString());
			Assert.Contains("Accuracy type: IsNominal", dt.ToString());
		}

		[Fact]
		public void TestDCString() {
			var dt = new DataCollection();

			output.WriteLine(dt.ToString());
			Assert.NotNull(dt.ToString());
		}

		[Fact]
		public void TestPDString() {
			var dt = new PeakDetection();

			output.WriteLine(dt.ToString());
			Assert.NotNull(dt.ToString());
		}

		[Fact]
		public void TestMDString() {
			var dt = new Ms1Deconvolution();

			output.WriteLine(dt.ToString());
			Assert.NotNull(dt.ToString());
		}

		[Fact]
		public void TestIString() {
			var dt = new Identification();

			output.WriteLine(dt.ToString());
			Assert.NotNull(dt.ToString());
		}

		[Fact]
		public void TestAString() {
			var dt = new Alignment();

			output.WriteLine(dt.ToString());
			Assert.NotNull(dt.ToString());
		}
	}
}
