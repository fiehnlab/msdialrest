﻿using Microsoft.Extensions.Logging;
using MsdialRunner;
using MsdialRunner.Exceptions;
using Xunit;

namespace MsdialRunnerTests
{
    public class MsdialRunnerTests {

        [Theory]
        [InlineData("", @"-o .\tmp", @"-m .\Resources\default-lcparams.txt" )]
        [InlineData(@"-i g:\data\LCMS\ABF", "", @"-m .\Resources\default-lcparams.txt")]
        [InlineData(@"-i g:\data\LCMS\ABF", @"-o .\tmp", "")]
        public void TestCallRunnerMissingParams_Throws_Exception(string input, string output, string method) {
            var exefile = @"h:\msdial\MsdialConsoleApp.exe";

            Assert.Throws<MsdialProcessException>(() => MsdialRun.Run(exefile, "lcmsdda", input, method, output, new LoggerFactory()));
        }
    }
}
