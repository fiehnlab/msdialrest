﻿using MsdialRunner.Model;
using Newtonsoft.Json;
using System.IO;
using Xunit;
using Xunit.Abstractions;

namespace MsdialRunnerTests {
    public class LCParameterTests {
		private readonly string schema = "msdial-lcschema.json";
		private readonly ITestOutputHelper output;

		public LCParameterTests(ITestOutputHelper output) {
			this.output = output;
		}

		private static readonly string jsonStr = File.ReadAllText("..\\..\\..\\Resources\\default-lcparams.json");

		[Fact]
		public void TestCanDeserializeJson() {
			var p = LCParameters.FromJson(jsonStr);

			output.WriteLine(p.ToString());
			Assert.NotNull(p);
			Assert.IsType<LCParameters>(p);
			Assert.NotEmpty(p.ToString());
			Assert.NotNull(p.DataType);
			Assert.NotNull(p.DataCollection);
			Assert.NotNull(p.CentroidParams);
			Assert.NotNull(p.LcPeakDetection);
			Assert.NotNull(p.Deconvolution);
			Assert.NotNull(p.LcIdentification);
			Assert.NotNull(p.Alignment);
		}

		[Fact]
		public void TestDTString() {
			var dt = new DataTypeParam() { DataType = "Centroid", IonMode = "Positive", AccuracyType = "IsNominal" };

			output.WriteLine(dt.ToString());
			Assert.Contains("Data type: Centroid", dt.ToString());
			Assert.Contains("Ion mode: Positive", dt.ToString());
			Assert.Contains("Accuracy type: IsNominal", dt.ToString());
		}

		[Fact]
		public void TestDCString() {
			var dt = new DataCollection();

			output.WriteLine(dt.ToString());
			Assert.NotNull(dt.ToString());
		}

		[Fact]
		public void TestCPString() {
			var cp = new CentroidParams();

			output.WriteLine(cp.ToString());
			Assert.NotNull(cp.ToString());
		}

		[Fact]
		public void TestPDString() {
			var dt = new LCPeakDetection();

			output.WriteLine(dt.ToString());
			Assert.NotNull(dt.ToString());
		}

		[Fact]
		public void TestMDString() {
			var dt = new Ms1Deconvolution();

			output.WriteLine(dt.ToString());
			Assert.NotNull(dt.ToString());
		}

		[Fact]
		public void TestIString() {
			var dt = new LCIdentification();

			output.WriteLine(dt.ToString());
			Assert.NotNull(dt.ToString());
		}

		[Fact]
		public void TestAString() {
			var dt = new LCAlignment();

			output.WriteLine(dt.ToString());
			Assert.NotNull(dt.ToString());
		}

		[Fact]
		public void TestMatchToSchema() {
			var lcParams = JsonConvert.DeserializeObject<LCParameters>(jsonStr);

			output.WriteLine(lcParams.ToString());
			Assert.IsAssignableFrom<LCParameters>(lcParams);
		}
	}
}
