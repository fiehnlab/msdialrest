﻿using Microsoft.AspNetCore.TestHost;
using MsdialRest.Model;
using MsdialRest.Model.Response;
using Newtonsoft.Json;
using System.IO;
using System.Net.Http;
using Xunit;
using Xunit.Abstractions;
using System.Net.Http.Headers;
using MsdialRunner.Exceptions;
using Microsoft.Extensions.Options;

namespace MsdialRestTests.Controllers {
    public class ScheduleControllerTests : ApiTests {
		public ScheduleControllerTests(ITestOutputHelper output) : base(output) {}

        [Fact]
		public void TestScheduleFromConversionIntegration() {
			HttpResponseMessage scResp = null;
			var sample = new Sample("testA.abf") { Format = "abf" };

			//prepare file as if it was uploaded
			var dest = Path.Combine(StorageFolder, sample.Filename);
			Directory.CreateDirectory(dest);
			File.Copy(Path.Combine("resources", "testA.abf"), Path.Combine(dest, sample.Filename), true);

            using (var host = new TestServer(webHostBuilder)) {
				using (var client = host.CreateClient()) {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(sample.Filename);
					scResp = client.GetAsync($"rest/deconvolution/schedule/{sample.Filename}").Result;
                    if (!scResp.IsSuccessStatusCode) {
                        output.WriteLine(scResp.ReasonPhrase);
                        output.WriteLine(scResp.Content.ReadAsStringAsync().Result);
                    }

                    var schedule = JsonConvert.DeserializeObject<ScheduleResponse>(scResp.Content.ReadAsStringAsync().Result);

                    try {
                        Assert.Equal("testA.abf", schedule.filename);
                        Assert.Equal(schedule.link, $"http://localhost/rest/deconvolution/status/{sample.Filename}");
                        Assert.True(File.Exists(Path.Combine(StorageFolder, sample.Filename, sample.Filename)));
                        Assert.True(File.Exists(Path.Combine(StorageFolder, sample.Filename, sample.Filename.Replace("abf", "msdial"))));
                    }catch(MsdialProcessException ex) {
                        output.WriteLine(ex.Message);
                        Assert.False(true);
                    } finally {
                        Directory.Delete(dest, true);
                    }
				}
			}
		}
    }
}
