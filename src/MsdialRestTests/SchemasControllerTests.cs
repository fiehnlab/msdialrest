﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Options;
using MsdialRest.Model;
using Xunit;
using Xunit.Abstractions;

namespace MsdialRestTests.Controllers {
	public class SchemasControllerTests : ApiTests {
		public SchemasControllerTests(ITestOutputHelper output) : base(output) {
        }

        [Fact]
		public void TestGetGcSchema() {
			using (var host = new TestServer(webHostBuilder)) {
				using (var client = host.CreateClient()) {
					var response = client.GetAsync("rest/deconvolution/schemas/gcschema").Result;
					response.EnsureSuccessStatusCode();
				}
			}
		}

		[Fact]
		public void TestGetLcSchema() {
			using (var host = new TestServer(webHostBuilder)) {
				using (var client = host.CreateClient()) {
					var response = client.GetAsync("rest/deconvolution/schemas/lcschema").Result;
					response.EnsureSuccessStatusCode();
				}
			}
		}
	}
}
