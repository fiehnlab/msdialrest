﻿using Microsoft.AspNetCore.TestHost;
using MsdialRest.Model;
using MsdialRest.Model.Response;
using Newtonsoft.Json;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using Xunit;
using Xunit.Abstractions;

namespace MsdialRestTests.Controllers {
    public class StatusControllerTests : ApiTests {

        public StatusControllerTests(ITestOutputHelper output) : base(output) { }

        [Fact]
        public void TestSucessfullProcess() {
            HttpResponseMessage statResp = null;
            var sample = new Sample("Urine.abf") { Format = ".abf" };
            var resFile = "Urine.msdial";

            //prepare file as if it was processed
            string dest = mockProcess(sample, resFile);

            using (var host = new TestServer(webHostBuilder)) {
                using (var client = host.CreateClient()) {
                    statResp = client.GetAsync($"rest/deconvolution/status/{sample.Filename}").Result;
                    if (!statResp.IsSuccessStatusCode) {
                        output.WriteLine(statResp.ReasonPhrase);
                        output.WriteLine(statResp.Content.ReadAsStringAsync().Result);
                    }

                    var status = JsonConvert.DeserializeObject<StatusResponse>(statResp.Content.ReadAsStringAsync().Result);

                    try {
                        Assert.Equal("Urine.msdial", status.filename);
                        Assert.Equal(status.link, $"http://localhost/rest/deconvolution/result/{sample.Filename}");
                        Assert.Equal(ProcessStatus.DECONVOLUTED, status.status);
                        Assert.True(File.Exists(Path.Combine(StorageFolder, sample.Filename, resFile)));
                    } finally {
                        Directory.Delete(Path.Combine(StorageFolder, sample.Filename), true);
                    }
                }
            }
        }

        [Fact]
        public void TestFileStillProcessing() {
            HttpResponseMessage statResp = null;
            var sample = new Sample("Urine.abf") { Format = ".abf" };
            var resFile = "Urine.msdial";

            //prepare file as if it was processed
            string dest = mockProcess(sample, resFile);

            //simulate file open while exporting
            var fs = File.OpenWrite(Path.Combine(dest, resFile));

            using (var host = new TestServer(webHostBuilder)) {
                using (var client = host.CreateClient()) {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(sample.Filename);
                    statResp = client.GetAsync($"rest/deconvolution/status/{sample.Filename}").Result;
                    if (!statResp.IsSuccessStatusCode) {
                        output.WriteLine(statResp.ReasonPhrase);
                        output.WriteLine(statResp.Content.ReadAsStringAsync().Result);
                    }

                    var status = JsonConvert.DeserializeObject<StatusResponse>(statResp.Content.ReadAsStringAsync().Result);

                    try {
                        Assert.Equal("Urine.msdial", status.filename);
                        Assert.Equal("", status.link);
                        Assert.Equal(ProcessStatus.PROCESSING, status.status);
                        Assert.True(File.Exists(Path.Combine(StorageFolder, sample.Filename, resFile)));
                    } finally {
                        fs.Dispose();
                        Directory.Delete(Path.Combine(StorageFolder, sample.Filename), true);
                    }
                }
            }
        }

        private string mockProcess(Sample sample, string resFile) {
            var dest = Path.Combine(StorageFolder, sample.Filename);
            if(Directory.Exists(dest)) { Directory.Delete(dest, true); }
            Directory.CreateDirectory(dest);
            File.Copy(Path.Combine("resources", resFile), Path.Combine(dest, resFile), true);
            return dest;
        }
    }
}
