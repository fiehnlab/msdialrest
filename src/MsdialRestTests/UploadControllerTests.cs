﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using MsdialRest.Model;
using MsdialRest.Model.Response;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Text.RegularExpressions;
using Xunit;
using Xunit.Abstractions;

namespace MsdialRestTests.Controllers {
    public class UploadControllerTests : ApiTests {
        private string guidRegex = @"^http://.*?/\w{10}$";

        public UploadControllerTests(ITestOutputHelper output) : base(output) { }

        [Fact]
        public void TestUploadConvertedIntegration() {
            using (var host = new TestServer(webHostBuilder)) {

                using (var client = host.CreateClient()) {
                    var sample = new Sample("GCTOF_QC6.cdf") { Format = "cdf" };
                    FileInfo file = new FileInfo(Path.Combine("resources", sample.Filename));
                    var response = client.PostAsync("rest/upload", MultipartFormDataContent(file.FullName)).Result;
                    var body = response.Content.ReadAsStringAsync().Result;

                    if (!response.IsSuccessStatusCode) {
                        output.WriteLine(response.StatusCode.ToString());
                        output.WriteLine(response.Content.ReadAsStringAsync().Result);
                    }

                    var upload = JsonConvert.DeserializeObject<UploadResponse>(response.Content.ReadAsStringAsync().Result);

                    try {
                        Assert.Equal("GCTOF_QC6.cdf", upload.filename);
                        Assert.True(Regex.Match(upload.link, guidRegex, RegexOptions.IgnoreCase).Success);
                        Assert.True(File.Exists(Path.Combine(StorageFolder, sample.Filename, sample.Filename)));
                    } finally {
                        if(Directory.Exists(Path.Combine(StorageFolder, sample.Filename)))
                            Directory.Delete(Path.Combine(StorageFolder, sample.Filename), true);
                    }
                }
            }
        }

        [Fact]
        public void TestUploadMSMS() {
            using (var host = new TestServer(webHostBuilder)) {
                using (var client = host.CreateClient()) {
                    var sample = new Sample("B5_SA0259_P20Lipids_Pos_1FV_2392_MSMS.abf") { Format = "abf" };
                    FileInfo file = new FileInfo(Path.Combine("J:/p20repo/abf/P20_pos_batch5_msms_abf", sample.Filename));

                    var response = client.PostAsync("rest/upload", MultipartFormDataContent(file.FullName)).Result;
                    var body = response.Content.ReadAsStringAsync().Result;

                    if (!response.IsSuccessStatusCode) {
                        output.WriteLine(response.StatusCode.ToString());
                        output.WriteLine(response.Content.ReadAsStringAsync().Result);
                    }

                    var upload = JsonConvert.DeserializeObject<UploadResponse>(response.Content.ReadAsStringAsync().Result);
                    output.WriteLine(JsonConvert.SerializeObject(upload));

                    try {
                        Assert.Equal("B5_SA0259_P20Lipids_Pos_1FV_2392_MSMS.abf", upload.filename);
                        Assert.True(Regex.Match(upload.link, guidRegex, RegexOptions.IgnoreCase).Success);
                        Assert.True(File.Exists(Path.Combine(StorageFolder, sample.Filename, sample.Filename)));
                    } finally {
                        if (Directory.Exists(Path.Combine(StorageFolder, sample.Filename)))
                            Directory.Delete(Path.Combine(StorageFolder, sample.Filename), true);
                    }
                }
            }
        }

        [Fact]
        public void TestUploadRawIntegration() {
            using (var host = new TestServer(webHostBuilder)) {
                using (var client = host.CreateClient()) {
                    var sample = new Sample("Urine.zip") { Format = "abf" };
                    FileInfo file = new FileInfo(Path.Combine("resources", sample.Filename));
                    var response = client.PostAsync("rest/upload", MultipartFormDataContent(file.FullName)).Result;
                    var body = response.Content.ReadAsStringAsync().Result;

                    if(!response.IsSuccessStatusCode) {
                        output.WriteLine(response.StatusCode.ToString());
                        output.WriteLine(response.Content.ReadAsStringAsync().Result);
                    }

                    var upload = JsonConvert.DeserializeObject<UploadResponse>(response.Content.ReadAsStringAsync().Result);

                    try {
                        Assert.Equal("Urine.wiff", upload.filename);
                        Assert.True(Regex.Match(upload.link, guidRegex, RegexOptions.IgnoreCase).Success);
                        Assert.True(File.Exists(Path.Combine(StorageFolder, sample.Filename, upload.filename)));
                    } finally {
                        if (Directory.Exists(Path.Combine(StorageFolder, sample.Filename)))
                            Directory.Delete(Path.Combine(StorageFolder, sample.Filename), true);
                    }
                }
            }
        }

        [Fact]
        public void TestUploadRawAgilentIntegration() {
            using (var host = new TestServer(webHostBuilder)) {
                using (var client = host.CreateClient()) {
                    var sample = new Sample("testA.d.zip") { Format = "zip" };
                    FileInfo file = new FileInfo(Path.Combine("resources", sample.Filename));
                    var response = client.PostAsync("rest/upload", MultipartFormDataContent(file.FullName)).Result;
                    var body = response.Content.ReadAsStringAsync().Result;

                    if (!response.IsSuccessStatusCode) {
                        output.WriteLine(response.StatusCode.ToString());
                        output.WriteLine(response.Content.ReadAsStringAsync().Result);
                    }

                    var upload = JsonConvert.DeserializeObject<UploadResponse>(response.Content.ReadAsStringAsync().Result);

                    try {
                        Assert.Equal("testA.d", upload.filename);
                        Assert.True(Regex.Match(upload.link, guidRegex, RegexOptions.IgnoreCase).Success);
                        Assert.True(Directory.Exists(Path.Combine(StorageFolder, sample.Filename, "testA.d")));
                    } finally {
                        if (Directory.Exists(Path.Combine(StorageFolder, sample.Filename)))
                            Directory.Delete(Path.Combine(StorageFolder, sample.Filename), true);
                    }
                }
            }

        }
    }
}
