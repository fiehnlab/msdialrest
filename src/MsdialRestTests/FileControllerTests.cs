﻿using Microsoft.AspNetCore.TestHost;
using MsdialRest.Model.Response;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http;
using Xunit;
using Xunit.Abstractions;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Hosting;

namespace MsdialRestTests.Controllers {
    public class FileControllerTests : ApiTests {

        public FileControllerTests(ITestOutputHelper output) : base(output) {}

        [Fact]
        public void TestFileExists() {
            HttpResponseMessage fileResp = null;
            var sample = "testA.abf";

            var dest = Path.Combine(StorageFolder, sample);
            Directory.CreateDirectory(dest);
            File.Copy(Path.Combine("resources", sample), Path.Combine(dest, sample));

            using (var host = new TestServer(webHostBuilder)) {
                using (var client = host.CreateClient()) {
                    fileResp = client.GetAsync($"rest/file/exists/{sample}").Result;
                    Directory.Delete(dest, true);

                    var content = fileResp.Content.ReadAsStringAsync().Result;

                    FileResponse resp = JsonConvert.DeserializeObject<FileResponse>(content);
                    output.WriteLine(resp.ToString());
                    Assert.Equal(HttpStatusCode.OK, fileResp.StatusCode);

                    Assert.True(resp.exists);
                    Assert.Equal(sample, resp.filename);
                }
            }
        }

        [Fact]
        public void TestFileNotExists() {
            HttpResponseMessage fileResp = null;
            var sample = "inexistent.file";

            using (var host = new TestServer(webHostBuilder)) {
                using (var client = host.CreateClient()) {
                    fileResp = client.GetAsync($"rest/file/exists/{sample}").Result;

                    Assert.Equal(HttpStatusCode.OK, fileResp.StatusCode);
                    var content = fileResp.Content.ReadAsStringAsync().Result;

                    FileResponse resp = JsonConvert.DeserializeObject<FileResponse>(content);
                    output.WriteLine(resp.ToString());

                    Assert.False(resp.exists);
                    Assert.Equal(sample, resp.filename);
                }
            }
        }

        [Fact]
        public void TestUploadRawFile() {
            HttpResponseMessage fileResp = null;
            FileInfo file = new FileInfo($"Resources{Path.DirectorySeparatorChar}testA.d.zip");

            using (var host = new TestServer(webHostBuilder)) {
                using (var client = host.CreateClient()) {
                    fileResp = client.PostAsync("rest/file/upload", MultipartFormDataContent(file.FullName)).Result;

                    Assert.Equal(HttpStatusCode.OK, fileResp.StatusCode);
                    var content = fileResp.Content.ReadAsStringAsync().Result;

                    UploadResponse resp = JsonConvert.DeserializeObject<UploadResponse>(content);

                    Directory.Delete(Path.Combine(StorageFolder, file.Name), true);
                    output.WriteLine($"Link: {resp.link}");

                    Assert.Equal(file.Name, resp.filename);
                    Assert.True(Regex.Match(resp.link, $".*/conversion/convert/{file.Name}", RegexOptions.IgnoreCase).Success);
                }
            }
        }

        [Fact]
        public void TestUploadConvertedFile() {
            HttpResponseMessage fileResp = null;
            FileInfo file = new FileInfo($"Resources{Path.DirectorySeparatorChar}testA.abf");

            using (var host = new TestServer(webHostBuilder)) {
                using (var client = host.CreateClient()) {
                    fileResp = client.PostAsync("rest/file/upload", MultipartFormDataContent(file.FullName)).Result;

                    Assert.Equal(HttpStatusCode.OK, fileResp.StatusCode);
                    var content = fileResp.Content.ReadAsStringAsync().Result;

                    UploadResponse resp = JsonConvert.DeserializeObject<UploadResponse>(content);
                    Directory.Delete(Path.Combine(StorageFolder, file.Name), true);
                    output.WriteLine($"Link: {resp.link}");

                    Assert.Equal(file.Name, resp.filename);
                    Assert.True(Regex.Match(resp.link, $".*/deconvolution/process/{file.Name}", RegexOptions.IgnoreCase).Success);
                }
            }
        }
    }
}
