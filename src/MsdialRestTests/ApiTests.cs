﻿using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Moq;
using MsdialRest.Model;
using Xunit.Abstractions;
using MsdialRest.Api;
using MsdialRest.Services;
using MsdialRest;
using FServerClient.Api;
using FServerClient.Model;
using System;
using MsdialRest.Utility.Unzip;

namespace MsdialRestTests {
    public abstract class ApiTests {
        protected readonly ITestOutputHelper output;

        protected IWebHostBuilder webHostBuilder;
        IConfigurationRoot Configuration { get; } = new ConfigurationBuilder().AddEnvironmentVariables().AddJsonFile("appsettings.Testing.json").Build();
        protected readonly string StorageFolder;
        protected LoggerFactory logFac = new LoggerFactory();
        protected string webRoot = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot");

        public ApiTests(ITestOutputHelper output) {
            this.output = output;

            webHostBuilder = new WebHostBuilder()
                .UseEnvironment("Testing")
                .UseConfiguration(Configuration)
                .UseWebRoot(webRoot)
                .UseStartup<Startup>();
            webHostBuilder
                .Configure(app => { app.UseMvc(); })
                .ConfigureServices(services => {
                    services.Configure<Config>(Configuration);
                    services.AddSingleton<IFilePrepService, FilePrepService>();
                    services.AddSingleton<IUnzipper, Unzipper>();
                    services.AddSingleton<IFServerClient, FServerCli>((ctx) => {
                        var config = Configuration.GetSection("Fserv");
                        var urlstr = config.GetValue<string>("FservHost") + ":" + config.GetValue<string>("FservPort");
                        Uri uri = new Uri(urlstr);
                        return new FServerCli(uri);
                    });
                    services.AddLogging();
                    services.AddMvc();
                }).ConfigureLogging(loggerFactory => {
                    loggerFactory.AddConsole(Configuration.GetSection("Logging"));
                    loggerFactory.AddDebug();
                });
            StorageFolder = Configuration.GetValue<string>("StorageFolder");
        }

        //helper methods
        protected static void CreateFileMock(out Mock<IFormFile> fileMock, out string filename, out MemoryStream ms) {
            fileMock = new Mock<IFormFile>();
            var content = "test file for testing purposes only... use at your own risk.";
            filename = "testFile.cdf";
            ms = new MemoryStream();
            var writer = new StreamWriter(ms);
            writer.Write(content);
            writer.Flush();
            ms.Position = 0;
        }

        protected static MultipartFormDataContent MultipartFormDataContent(string testFile) {
            var multiPartContent = new MultipartFormDataContent("boundary=---I+Hate_this=Boundary:thing");

            var fileStream = new FileStream(testFile, FileMode.Open, FileAccess.Read);
            var streamContent = new StreamContent(fileStream);
            streamContent.Headers.ContentType = new MediaTypeHeaderValue("multipart/form-data");

            // Add the file key/value
            multiPartContent.Add(streamContent, "file", testFile.Substring(testFile.LastIndexOf("/") + 1));

            return multiPartContent;
        }


        protected string GetPath(string url) {
            return url.Replace("http://localhost/", "");
        }
    }
}
