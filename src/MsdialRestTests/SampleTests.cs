﻿using MsdialRest.Model;
using Xunit;

namespace MsdialRestTests {
    public class SampleTests {

        [Fact]
        public void TestCreateSample() {
            var samplename = "testSample";
            var sample = new Sample(samplename);
            var sample2 = new Sample(samplename);
            var sample3 = new Sample("not_the-samesample");

            Assert.IsType<Sample>(sample);
            Assert.NotNull(sample);
            Assert.Equal("dafe9ccce3", sample.Id);
            Assert.Equal(samplename, sample.Filename);
            Assert.Equal(sample.Id, sample2.Id);
            Assert.NotEqual(sample.Id, sample3.Id);
        }
    }
}
