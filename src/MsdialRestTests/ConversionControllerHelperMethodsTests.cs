﻿using MsdialRest.Controllers;
using Moq;
using Xunit;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MsdialRest.Model;
using System.IO;
using System.Diagnostics;
using Xunit.Abstractions;
using System;

namespace MsdialRestTests.Controllers {
    public class ConversionControllerHelperMethodsTests : ApiTests {
        private readonly Mock<IHostingEnvironment> env = new Mock<IHostingEnvironment>();
        private readonly IOptions<Config> config;
        private readonly MyConversionController controller;

        public ConversionControllerHelperMethodsTests(ITestOutputHelper output, IOptions<Config> config) : base(output) {
            env.Setup(x => x.ApplicationName).Returns("Integration Test");
            env.Setup(x => x.EnvironmentName).Returns("Test");
            env.Setup(x => x.ContentRootPath).Returns(Directory.GetCurrentDirectory());
            env.Setup(x => x.WebRootPath).Returns(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot"));

            this.config = config;
            output.WriteLine($"============== config ==============\n{config.Value.StorageFolder}");
            controller = new MyConversionController(env.Object, logFac, this.config);
        }

        //[Fact(Skip = "find a way to run this")]
        public void TestGetValidRawFile() {
            var dest = Path.Combine(StorageFolder, "Test");
            output.WriteLine($"dest {dest}");
            Directory.CreateDirectory(Path.Combine(dest, "raw"));
            File.Copy(@"Resources/TestFile.d", Path.Combine(dest, "raw", "TestFile.d"), true);
            var resfile = controller.GetRawFile("Test");
            Assert.Equal(Path.Combine(dest, "raw", "TestFile.d"), resfile);

            Directory.Delete(dest, true);
        }

        //[Fact(Skip = "find a way to run this")]
        public void TestGetFileFromInexistentFolder() {
            Assert.Throws<DirectoryNotFoundException>(() => controller.GetRawFile("BadTest"));
        }

        //[Fact(Skip = "find a way to run this")]
        public void TestGetInexistentFile() {
            Directory.CreateDirectory(Path.Combine(StorageFolder, "TestEmpty", "raw"));
            Assert.Throws<InvalidOperationException>(() => controller.GetRawFile("TestEmpty"));
            Directory.Delete(Path.Combine(StorageFolder, "TestEmpty"), true);
        }
    }

    class MyConversionController : ConversionController {
        public MyConversionController(IHostingEnvironment _env, ILoggerFactory _loggerFactory, IOptions<Config> _config) : base(_env, _loggerFactory, _config) {
            Debug.WriteLine(_env);
        }

        public new string GetRawFile(string folder) {
            try {
                return base.GetRawFile(folder);
            } catch (Exception ex) {
                throw ex;
            }
        }
    }
}
