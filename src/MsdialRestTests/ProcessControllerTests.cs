﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using MsdialRest.Model;
using MsdialRest.Model.Response;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Net.Http;
using Xunit;
using Xunit.Abstractions;

namespace MsdialRestTests.Controllers {
    public class ProcessControllerTests : ApiTests {

        public ProcessControllerTests(ITestOutputHelper output) : base(output) {}

        [Fact]
        public void TestProcessGoodFile() {
            HttpResponseMessage procResp = null;
            var sample = new Sample("testA.abf") { Format = "abf" };
            output.WriteLine($"Storage: {StorageFolder}");

            var dest = Path.Combine(StorageFolder, sample.Filename);
            Directory.CreateDirectory(dest);
            File.Copy(Path.Combine("resources", sample.Filename), Path.Combine(dest, sample.Filename));

            using (var host = new TestServer(webHostBuilder)) {
                using (var client = host.CreateClient()) {
                    procResp = client.GetAsync($"rest/deconvolution/process/{sample.Filename}").Result;
                    var content = procResp.Content.ReadAsStringAsync().Result;

                    ProcessResponse resp = JsonConvert.DeserializeObject<ProcessResponse>(content);
                    output.WriteLine(resp.ToString());

                    Directory.Delete(dest, true);

                    Assert.Equal(HttpStatusCode.OK, procResp.StatusCode);

                    Assert.Equal("testA.msdial", resp.filename);
                    Assert.Matches($"^http://.*?/rest/file/download/{resp.filename}$", resp.link);
                }
            }

        }

        [Fact]
        public void TestProcessInexistentFile() {
            HttpResponseMessage procResp = null;
            var sample = new Sample("wrongfile.abf") { Format = "abf" };

            using (var host = new TestServer(webHostBuilder)) {
                using (var client = host.CreateClient()) {
                    procResp = client.GetAsync($"rest/deconvolution/process/{sample.Filename}").Result;
                    output.WriteLine($"response: {procResp.StatusCode} - {procResp.ReasonPhrase}");
                    Assert.Equal(HttpStatusCode.BadRequest, procResp.StatusCode);

                    ProcessResponse resp = JsonConvert.DeserializeObject<ProcessResponse>(procResp.Content.ReadAsStringAsync().Result);
                    output.WriteLine(resp.ToString());

                    Assert.Equal(sample.Filename, resp.filename);
                    Assert.Equal("Not found, please upload", resp.error);
                    Assert.Null(resp.link);
                }
            }
        }

        [Fact]
        public void TestProcessBadFile() {
            HttpResponseMessage procResp = null;
            var sample = new Sample("wrongfile.txt") { Format = "txt" };

            using (var host = new TestServer(webHostBuilder)) {
                using (var client = host.CreateClient()) {
                    procResp = client.GetAsync($"rest/deconvolution/process/{sample.Filename}").Result;
                    output.WriteLine($"response: {procResp.StatusCode} - {procResp.ReasonPhrase}");
                    Assert.Equal(HttpStatusCode.BadRequest, procResp.StatusCode);

                    ProcessResponse resp = JsonConvert.DeserializeObject<ProcessResponse>(procResp.Content.ReadAsStringAsync().Result);
                    output.WriteLine(resp.ToString());

                    Assert.Equal(sample.Filename, resp.filename);
                    Assert.Equal("invalid file type, please use .abf; .raw; zipped agilent .d or zipped sciex .wiff", resp.error);
                    Assert.Null(resp.link);
                }
            }
        }
    }
}
