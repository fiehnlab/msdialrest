﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using MsdialRest.Model;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Xunit;
using Xunit.Abstractions;

namespace MsdialRestTests.Controllers {
    public class ResultControllerTests : ApiTests {
        public ResultControllerTests(ITestOutputHelper output) : base(output) {}

        [Fact]
        public void TestDownloadResult() {
            HttpResponseMessage result = null;
            var sample = new Sample("Urine.abf") { Format = ".abf" };
            var resFile = "Urine.msdial";

            //prepare file as if it was processed
            var dest = mockProcess(sample, resFile);

            using (var host = new TestServer(webHostBuilder)) {
                using (var client = host.CreateClient()) {
                    result = client.GetAsync($"rest/deconvolution/result/{sample.Filename}").Result;

                    var length = result.Content.Headers.ContentLength;
                    FileInfo downloaded;

                    if (result.StatusCode.Equals(HttpStatusCode.OK)) {
                        var file = result.Content.ReadAsStreamAsync().Result;
                        downloaded = new FileInfo(Path.Combine(Directory.GetCurrentDirectory(), "downloaded.msdial"));
                        file.CopyTo(downloaded.OpenWrite());

                        try {
                            Assert.True(File.Exists(downloaded.FullName));
                            Assert.Equal(length, downloaded.Length);
                        } finally {
                            Directory.Delete(dest, true);
                        }
                    }
                }
            }
        }

        [Fact]
        public void TestLockedFile() {
            HttpResponseMessage result = null;
            var sample = new Sample("Urine.abf") { Format = ".abf" };
            var resFile = "Urine.msdial";

            //prepare file as if it was processed
            var dest = mockProcess(sample, resFile);

            //simulate file being opened
            var fs = new FileInfo(Path.Combine(dest, resFile)).OpenWrite();

            using (var host = new TestServer(webHostBuilder)) {
                using (var client = host.CreateClient()) {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(sample.Filename);
                    result = client.GetAsync($"rest/deconvolution/result/{sample.Filename}").Result;

                    try {
                        Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
                    } finally {
                        fs.Dispose();
                        Directory.Delete(dest, true);
                    }
                }
            }
        }

        private string mockProcess(Sample sample, string resFile) {
            var dest = Path.Combine(StorageFolder, sample.Filename);
            if (Directory.Exists(dest)) { Directory.Delete(dest, true); }
            Directory.CreateDirectory(dest);
            File.Copy(Path.Combine("resources", resFile), Path.Combine(dest, resFile), true);
            return dest;
        }
    }
}
