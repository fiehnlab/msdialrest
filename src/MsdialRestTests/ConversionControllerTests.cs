﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using MsdialRest.Model;
using MsdialRest.Model.Response;
using MsdialRest.Utility.Unzip;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using Xunit;
using Xunit.Abstractions;

namespace MsdialRestTests.Controllers {
    public class ConversionControllerTests : ApiTests {
       public ConversionControllerTests(ITestOutputHelper output ) : base(output) { }

        [Fact]
        public void TestAgilentConversion() {
            var name = "testA.d.zip";
            var authname = "testA.d";
            PrepUpload(out HttpResponseMessage convRest, name, ".zip", out Sample sample);

            using (var host = new TestServer(webHostBuilder)) {
                using (var client = host.CreateClient()) {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(authname);

                    convRest = client.GetAsync($"rest/conversion/convert/{sample.Filename}").Result;
                    if (!convRest.IsSuccessStatusCode) {
                        output.WriteLine(convRest.ReasonPhrase);
                        output.WriteLine(convRest.Content.ReadAsStringAsync().Result);
                    }

                    var conversion = JsonConvert.DeserializeObject<ConversionResponse>(convRest.Content.ReadAsStringAsync().Result);

                    try {
                        Assert.Equal("testA.d", conversion.filename);
                        Assert.Equal($"http://localhost/rest/deconvolution/schedule/{sample.Filename}", conversion.link);
                        Assert.True(File.Exists(Path.Combine(StorageFolder, sample.Filename, "testA.abf")));
                    } finally {
                        Directory.Delete(Path.Combine(StorageFolder, sample.Filename), true);
                    }
                }
            }
        }

        [Fact]
        public void TestValidAuthorization() {
            var name = "Urine.zip";
            PrepUpload(out HttpResponseMessage convRest, name, ".zip", out Sample sample);

            using (var host = new TestServer(webHostBuilder)) {
                using (var client = host.CreateClient()) {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(sample.Filename);

                    convRest = client.GetAsync($"rest/conversion/convert/{sample.Filename}").Result;
                    if (!convRest.IsSuccessStatusCode) {
                        output.WriteLine(convRest.ReasonPhrase);
                        output.WriteLine(convRest.Content.ReadAsStringAsync().Result);
                    }

                    var conversion = JsonConvert.DeserializeObject<ConversionResponse>(convRest.Content.ReadAsStringAsync().Result);

                    try {
                        Assert.Equal("Urine.wiff", conversion.filename);
                        Assert.Equal($"http://localhost/rest/deconvolution/schedule/{sample.Filename}", conversion.link);
                        Assert.True(File.Exists(Path.Combine(StorageFolder, sample.Filename, "Urine.abf")));
                    } finally {
                        Directory.Delete(Path.Combine(StorageFolder, sample.Filename), true);
                    }
                }
            }
        }

        private void PrepUpload(out HttpResponseMessage convRest, string name, string format, out Sample sample) {
            convRest = null;
            sample = new Sample(name) { Format = format };

            //prepare file as if it was uploaded
            var dest = Path.Combine(StorageFolder, sample.Filename);
            Directory.CreateDirectory(dest);

            new Unzipper().Unzip(Path.Combine("resources", name), dest);
        }

    }
}
