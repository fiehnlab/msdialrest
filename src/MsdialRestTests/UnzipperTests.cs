﻿using MsdialRest.Utility.Unzip;
using System.IO;
using Xunit;
using Xunit.Abstractions;

namespace MsdialRestTests.Utility {
    public class UnzipperTests {
		ITestOutputHelper output;
        private readonly string unzipDir = "unziptest";

        public UnzipperTests(ITestOutputHelper output) {
			this.output = output;
		}

        [Fact]
        public void TestUnzippingSciexFile() {
            var input = @"Resources\Urine.zip";
            var outpath = Path.Combine(Directory.GetCurrentDirectory(), unzipDir);
			if (Directory.Exists(outpath)) Directory.Delete(outpath, true);
            output.WriteLine($"outpath: {outpath}");
			var res = new Unzipper().Unzip(input, outpath);

			Assert.Equal(Path.Combine(outpath, "Urine.wiff"), res);
			Assert.True(File.Exists(Path.Combine(outpath, "Urine.wiff")));

            Directory.Delete(outpath, true);
        }

        [Fact]
        public void TestUnzippingAgilentFile() {
            var input = @"Resources/testA.d.zip";
            var outpath = Path.Combine(Directory.GetCurrentDirectory(), unzipDir);
            output.WriteLine($"outpath: {outpath}");
            if (Directory.Exists(outpath)) Directory.Delete(outpath, true);

			var res = new Unzipper().Unzip(input, outpath);

			Assert.Equal(Path.Combine(outpath, "testA.d"), res);
			Assert.True(Directory.Exists(Path.Combine(outpath, "testA.d")));

            Directory.Delete(outpath, true);
        }
    }
}
