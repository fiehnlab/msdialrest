﻿using Microsoft.AspNetCore.TestHost;
using System.IO;
using System.Net;
using Xunit;
using Xunit.Abstractions;

namespace MsdialRestTests.Controllers {
    public class DeleteControllerTests : ApiTests {

        public DeleteControllerTests(ITestOutputHelper output) : base(output) {}

        [Fact]
        public void TestDeleteInexistentFile() {
            var inexistentFile = "i_do_not_exist";
            using (var host = new TestServer(webHostBuilder)) {
                using (var client = host.CreateClient()) {
                    var response = client.DeleteAsync($"rest/deconvolution/cleanup/{inexistentFile}").Result;
                    Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
                }
            }
        }

        [Fact]
        public void TestDeleteRealFile() {
            var filename = "test_file.abf";
            var dest = Path.Combine(StorageFolder, filename);
            Directory.CreateDirectory(dest);

            //create some fake content representing raw, processed and results data
            File.CreateText(Path.Combine(dest, filename)).Dispose();

            using (var host = new TestServer(webHostBuilder)) {
                using (var client = host.CreateClient()) {
                    var response = client.DeleteAsync($"rest/deconvolution/cleanup/{filename}").Result;
                    Assert.Equal(HttpStatusCode.OK, response.StatusCode);
                }
            }
        }
    }
}
