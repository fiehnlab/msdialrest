﻿using Microsoft.Extensions.Logging;
using MsdialRunner.Exceptions;
using MsdialRunner.Model;
using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;

namespace MsdialRunner {
    public class MsdialRun {
        private static ILogger logger;

        /// <summary>
        /// Calls msdial console app to process "file"
        /// </summary>
        /// <param name="method">One of gcms, lcmsdda or lcmsdia</param>
        /// <param name="file">Full path of the file to process</param>
        /// <param name="output">Full path of the results folder</param>
        /// <param name="parameters">Full path of the parameters file</param>
        public static MsdialResponse Run(string msdial, string method, string ifile, string output, string parameters, ILoggerFactory log) {
            logger = log.CreateLogger<MsdialRun>();

            if (string.IsNullOrEmpty(ifile)) throw new MsdialProcessException("Parameter 'file' cannot be empty");
            if (string.IsNullOrEmpty(output)) throw new MsdialProcessException("Parameter 'output' cannot be empty");
            if (string.IsNullOrEmpty(parameters)) throw new MsdialProcessException("Parameter 'parameters' cannot be empty");

            if (!Directory.Exists(output)) {
                Directory.CreateDirectory(output);
            }

			if(RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) {
				ifile = ifile.Replace("/","\\");
				output = output.Replace("/", "\\");
				parameters = parameters.Replace("/", "\\");
			}

            var file = File.GetAttributes(ifile).HasFlag(FileAttributes.Directory) ? ifile : new FileInfo(ifile).Directory.FullName;

            ProcessStartInfo startInfo = new ProcessStartInfo(msdial);
			startInfo.CreateNoWindow = false;
			startInfo.UseShellExecute = false;
			startInfo.RedirectStandardOutput = true;
			startInfo.RedirectStandardError = true;
			startInfo.WorkingDirectory = msdial.Substring(0, msdial.LastIndexOf("\\"));
			startInfo.Arguments = $"{method} -i {file} -o {output} -m {parameters}";

			var stdout="";
			var stderr="";
			MsdialResponse resp;

            try {
                // Start the process with the info we specified.
                // Call WaitForExit and then the using-statement will close.

                Process exeProcess = Process.Start(startInfo);
                stdout = exeProcess.StandardOutput.ReadToEnd();
                stderr = exeProcess.StandardError.ReadToEnd();
                exeProcess.WaitForExit();

                if (exeProcess.ExitCode != 0) {
                    throw new MsdialProcessException() { MsdialCode = exeProcess.ExitCode, MsdialOutput = stdout, MsdialError = stderr, Source = typeof(MsdialRun).FullName };
                }

                if (!string.IsNullOrEmpty(stderr)) {
                    throw new MsdialProcessException() { MsdialCode = exeProcess.ExitCode, MsdialError = stderr, MsdialOutput = output };
                }

                resp = new MsdialResponse() { code = exeProcess.ExitCode, message = "Output: " + stdout + "\nError: " + stderr };
                exeProcess.Dispose();

                return resp;
            } catch (Exception ex) {
                switch (ex.GetType().Name) {
                    case "MsdialProcessException":
                        logger.LogError($"Error runnig MS-DIAL: {ex.Message}\nProcess output: {stdout}\nProcess error: {stderr}");
                        throw (ex);
                    default:
                        logger.LogError($"Error runnig MS-DIAL: {ex.Message}\nProcess output: {stdout}\nProcess error: {stderr}");
                        throw new MsdialProcessException(ex.Message, ex);
                }
            }
		}
	}
}
