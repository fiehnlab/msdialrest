﻿namespace MsdialRunner.Model {
	public interface IParameters {
		string GetVersion();
    }

	public interface IParameterSection {
		string GetName();
		string ToString();
	}

	public interface IDataTypeParam : IParameterSection { }
	public interface IDataCollection : IParameterSection { }
	public interface IPeakDetection : IParameterSection { }
	public interface IMs1Deconvolution : IParameterSection { }
	public interface IIdentification : IParameterSection { }
	public interface IAlignment : IParameterSection { }
	public interface ICentroidParams : IParameterSection { }
	public interface IFilePaths : IParameterSection { }
}
