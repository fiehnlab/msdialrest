﻿namespace MsdialRunner.Model {
	public class MsdialResponse {
		private int _code = 0;
		private string _message = "";

		public int code {
			get {
				return _code;
			}

			set {
				_code = value;
			}
		}

		public string message {
			get {
				return _message;
			}

			set {
				_message = value;
			}
		}
	}
}
