﻿using System;
using Newtonsoft.Json;
using System.Text;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace MsdialRunner.Model {
	[JsonObject]
	public class LCParameters {
		public string GetVersion() => "2.58";

		private IDataTypeParam dataType = new LCDataType();
		private IDataCollection dataCollection = new DataCollection();
		private ICentroidParams centroidParams = new CentroidParams();
		private IPeakDetection lcPeakDetection = new LCPeakDetection();
		private IMs1Deconvolution deconvolution = new LCDeconvolution() { SigmaWindowValue = 0.0001, AmplitudeCutOff = 10 };
		private IIdentification lcIdentification = new LCIdentification();
		private IAlignment alignment = new LCAlignment();

		[JsonProperty]
		public LCDataType DataType { get; set; }
		[JsonProperty]
		public DataCollection DataCollection { get; set; }
		[JsonProperty]
		public CentroidParams CentroidParams { get; set; }
		[JsonProperty]
		public LCPeakDetection LcPeakDetection { get; set; }
		[JsonProperty]
		public LCDeconvolution Deconvolution { get; set; }
		[JsonProperty(Required = Required.Default)]
		public LCIdentification LcIdentification { get; set; }
		[JsonProperty(Required = Required.Default)]
		public Alignment Alignment { get; set; }

		public static LCParameters FromJson(string jsonStr) {
			return JObject.Parse(jsonStr).ToObject<LCParameters>();
		}

		public static LCParameters GetDefaultParameters() {
			return new LCParameters();
		}

		public override string ToString() {
			var paramStr = new StringBuilder();

			paramStr.AppendLine(DataType?.GetName()).AppendLine(DataType?.ToString())
				.AppendLine(DataCollection?.GetName()).AppendLine(DataCollection?.ToString())
				.AppendLine(CentroidParams?.GetName()).AppendLine(CentroidParams?.ToString())
				.AppendLine(LcPeakDetection?.GetName()).AppendLine(LcPeakDetection?.ToString())
				.AppendLine(Deconvolution?.GetName()).AppendLine(Deconvolution?.ToString());

			if (LcIdentification?.ToString() != null) {
				paramStr.AppendLine(LcIdentification?.GetName()).AppendLine(LcIdentification?.ToString());
			}
			if (Alignment?.ToString() != null) {
				paramStr.AppendLine(Alignment?.GetName()).AppendLine(Alignment?.ToString());
			}

			return paramStr.ToString();
		}
	}

	[JsonObject]
	public class LCDataType : IDataTypeParam {
		private string ms1DataType;
		private string ms2DataType;
		private string ionMode;
		private string diaFile;

		[JsonProperty]
		public string Ms1DataType {
			get { return ms1DataType; }
			set {
				if (value.ToLower().Equals("Centroid") ||
					value.ToLower().Equals("Profile")) {
					ms1DataType = string.Concat(value.Substring(0, 1).ToUpper(), value.Substring(1).ToLower());
				} else {
					ms1DataType = "Centroid";
				}
			}
		}
		[JsonProperty]
		public string Ms2DataType {
			get { return ms2DataType; }
			set {
				if (value.ToLower().Equals("Centroid") ||
					value.ToLower().Equals("Profile")) {
					ms2DataType = string.Concat(value.Substring(0, 1).ToUpper(), value.Substring(1).ToLower());
				} else {
					ms2DataType = "Centroid";
				}
			}
		}
		[JsonProperty]
		public string IonMode {
			get { return ionMode; }
			set {
				if (value.ToLower().Equals("Positive") ||
					value.ToLower().Equals("Negative")) {
					ionMode = string.Concat(value.Substring(0, 1).ToUpper(), value.Substring(1).ToLower());
				} else {
					ionMode = "Positive";
				}
			}
		}
		[JsonProperty(Required = Required.Default)]
		public string DiaFile { get; set; }

		public string GetName() => "#Data type";

		public override string ToString() {
			var str = new StringBuilder();
			str.Append("MS1 data type: ").AppendLine(Ms1DataType)
				.Append("MS2 data type: ").AppendLine(Ms2DataType)
				.Append("Ion mode: ").AppendLine(IonMode)
				.Append("DIA file: ").AppendLine(DiaFile);
			return str.ToString();
		}
	}

	[JsonObject]
	public class CentroidParams : ICentroidParams {
		private double ms1tolerance = 0.01;
		private double ms2tolerance = 0.05;

		[JsonProperty]
		public double Ms1Tolerance {
			get { return ms1tolerance; }
			set {
				if (value < 0.0) { value = 0.0; }
				ms1tolerance = value;
			}
		}
		[JsonProperty]
		public double Ms2Tolerance {
			get { return ms2tolerance; }
			set {
				if (value < 0.0) { value = 0.0; }
				ms2tolerance = value;
			}
		}

		public string GetName() => "#Centroid Parameters";

		public override string ToString() {
			var str = new StringBuilder();
			str.Append("MS1 tolerance for centroid: ").AppendLine(Ms1Tolerance.ToString())
				.Append("MS2 tolerance for centroid: ").AppendLine(Ms2Tolerance.ToString());
			return str.ToString();
		}
	}

	[JsonObject]
	public class LCPeakDetection : IPeakDetection {
		private List<string> methods = new List<string>() { "SimpleMovingAverage", "LinearWeightedMovingAverage", "SavitzkyGolayFilter", "BinomialFilter" };

		private string smoothingMethod = "LinearWeightedMovingAverage";
		private int smoothingLevel = 1;
		private int minimumDatapoints = 5;
		private int minimumAmplitude = 1000;
		private double massSliceWidth = 0.1;

		[JsonProperty]
		public string SmoothingMethod {
			get => smoothingMethod;
			set => smoothingMethod = methods.Contains(value)?value: "LinearWeightedMovingAverage";
		}
		[JsonProperty]
		public int SmoothingLevel {
			get => smoothingLevel;
			set => smoothingLevel = value < 0 ? 0 : value;
		}
		[JsonProperty]
		public int MinimumDatapoints {
			get => minimumDatapoints;
			set => minimumDatapoints = value < 0 ? 0 : value;
		}
		[JsonProperty]
		public int MinimumAmplitude {
			get => minimumAmplitude;
			set => minimumAmplitude = value < 0 ? 0 : value;
		}
		[JsonProperty]
		public double MassSliceWidth {
			get => massSliceWidth;
			set => massSliceWidth = value < 0.0 ? 0.0 : value;
		}

		public string GetName() => "#Peak detection parameters";

		public override string ToString() {
			var str = new StringBuilder();
			str.Append("Smoothing method: ").AppendLine(SmoothingMethod)
				.Append("Smoothing level: ").AppendLine(SmoothingLevel.ToString())
				.Append("Minimum peak width: ").AppendLine(MinimumDatapoints.ToString())
				.Append("Minimum peak height: ").AppendLine(MinimumAmplitude.ToString())
				.Append("Mass slice width: ").AppendLine(MassSliceWidth.ToString());
			return str.ToString();
		}
	}

	[JsonObject]
	public class LCDeconvolution : Ms1Deconvolution {
		new public string GetName() => "#Deconvolution parameters";
	}

	[JsonObject]
	public class LCIdentification : IIdentification {
		private string mspFile = "";
		private double rtToleranceForId = 0.5;
		private double accMs1ToleranceforId = 0.025;
		private double accMs2ToleranceforId = 0.25;
		private int idScoreCutoff = 70;
		private string textFile = "";
		private double rtToleranceforPostId = 0.25;
		private double accMs1ToleranceForPostId = 0.01;
		private int postIdScoreCutoff = 85;

		[JsonProperty(Required = Required.Default)]
		public string MspFile { get => mspFile; set => mspFile = value; }
		[JsonProperty]
		public double RtToleranceForId { get => rtToleranceForId; set => rtToleranceForId = value < 0 ? 0 : value; }
		[JsonProperty]
		public double AccMs1ToleranceforId { get => accMs1ToleranceforId; set => accMs1ToleranceforId = value < 0 ? 0 : value; }
		[JsonProperty]
		public double AccMs2ToleranceforId { get => accMs2ToleranceforId; set => accMs2ToleranceforId = value < 0 ? 0 : value; }
		[JsonProperty]
		public int IdScoreCutoff { get => idScoreCutoff; set => idScoreCutoff = value < 0 ? 0 : value; }
		[JsonProperty(Required = Required.Default)]
		public string TextFile { get => textFile; set => textFile = value; }
		[JsonProperty]
		public double RtToleranceforPostId { get => rtToleranceforPostId; set => rtToleranceforPostId = value < 0 ? 0 : value; }
		[JsonProperty]
		public double AccMs1ToleranceForPostId { get => accMs1ToleranceForPostId; set => accMs1ToleranceForPostId = value < 0 ? 0 : value; }
		[JsonProperty]
		public int PostIdScoreCutoff { get => postIdScoreCutoff; set => postIdScoreCutoff = value < 0 ? 0 : value; }

		public string GetName() => "#MSP file and MS/MS identification setting";

		public override string ToString() {
			var str = new StringBuilder();
			str.Append("MSP file: ").AppendLine(MspFile)
				.Append("Retention time tolerance for identification: ").AppendLine(RtToleranceForId.ToString())
				.Append("Accurate ms1 tolerance for identification: ").AppendLine(AccMs1ToleranceforId.ToString())
				.Append("Accurate ms2 tolerance for identification: ").AppendLine(AccMs2ToleranceforId.ToString())
				.Append("Identification score cut off: ").AppendLine(IdScoreCutoff.ToString())
				.AppendLine()
				.AppendLine("# Text file and post identification (retention time and accurate mass based) setting")
				.Append("Text file: ").AppendLine(TextFile)
				.Append("Retention time tolerance for post identification: ").AppendLine(RtToleranceforPostId.ToString())
				.Append("Accurate ms1 tolerance for post identification: ").AppendLine(AccMs1ToleranceForPostId.ToString())
				.Append("Post identification score cut off: ").AppendLine(PostIdScoreCutoff.ToString());
			return str.ToString();
		}
	}

	[JsonObject]
	public class LCAlignment : Alignment {
		public override string ToString() {
			var str = new StringBuilder();
			str.Append("Retention time tolerance for alignment: ").AppendLine(RetentionTimeTolerance.ToString())
				.Append("MS1 tolerance for alignment: ").AppendLine(EISimilarityTolerance.ToString())
				.Append("Retention time factor for alignment: ").AppendLine(RetentionTimeFactor.ToString())
				.Append("MS1 factor for alignment: ").AppendLine(EISimilarityFactor.ToString())
				.Append("Peak count filter: ").AppendLine(PeakCountFilter.ToString())
				.Append("QC at least filter: ").AppendLine(QCAtLeastFilter ? "True" : "False");

			return str.ToString();
		}
	}
}
