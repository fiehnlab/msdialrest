﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace MsdialRunner.Model {
	[JsonObject]
	public class GCParameters {
		public string GetVersion() { return "2.50"; }

		private IDataTypeParam dataType = new DataTypeParam();
		private IDataCollection dataCollection = new DataCollection();
		private IPeakDetection peakDetection = new PeakDetection();
		private IMs1Deconvolution deconvolution = new Ms1Deconvolution();
		private IIdentification identification = new Identification();
		private IAlignment alignment = new Alignment();

		[JsonProperty]
		public DataTypeParam DataType { get; set; }
		[JsonProperty]
		public DataCollection DataCollection { get; set; }
		[JsonProperty]
		public PeakDetection PeakDetection { get; set; }
		[JsonProperty]
		public Ms1Deconvolution Deconvolution { get; set; }
		[JsonProperty(Required = Required.Default)]
		public Identification Identification { get; set; }
		[JsonProperty(Required = Required.Default)]
		public Alignment Alignment { get; set; }

		private GCParameters() {}

		public static GCParameters FromJson(string jsonStr) {
			return JObject.Parse(jsonStr).ToObject<GCParameters>();
		}

		public static GCParameters GetDefaultParameters() {
			return new GCParameters();
		}

		public override string ToString() {
			var paramStr = new StringBuilder();

			paramStr.AppendLine(DataType?.GetName()).AppendLine(DataType?.ToString())
				.AppendLine(DataCollection?.GetName()).AppendLine(DataCollection?.ToString())
				.AppendLine(PeakDetection?.GetName()).AppendLine(PeakDetection?.ToString())
				.AppendLine(Deconvolution?.GetName()).AppendLine(Deconvolution?.ToString());

			if (Identification?.ToString() != null) {
				paramStr.AppendLine(Identification?.GetName()).AppendLine(Identification?.ToString());
			}
			if (Alignment?.ToString() != null) {
				paramStr.AppendLine(Alignment?.GetName()).AppendLine(Alignment?.ToString());
			}

			return paramStr.ToString();
		}
	}

	[JsonObject]
	public class DataTypeParam : IDataTypeParam {
		private string dataType;
		private string ionMode;
		private string accuracyType;

		[JsonProperty(PropertyName = "datatype")]
		public string DataType {
			get { return dataType; }
			set {
				if (value.ToLower().Equals("Centroid") ||
					value.ToLower().Equals("Profile")) {
					dataType = string.Concat(value.Substring(0,1).ToUpper(),value.Substring(1).ToLower());
				} else {
					dataType = "Centroid";
				}
			}
		}
		[JsonProperty(PropertyName = "ionmode")]
		public string IonMode {
			get { return ionMode; }
			set {
				if (value.ToLower().Equals("Positive") ||
					value.ToLower().Equals("Negative")) {
					ionMode = string.Concat(value.Substring(0, 1).ToUpper(), value.Substring(1).ToLower());
				} else {
					ionMode = "Positive";
				}
			}
		}
		[JsonProperty(PropertyName = "accuracy")]
		public string AccuracyType {
			get { return accuracyType; }
			set {
				if (value.ToLower().Equals("isnominal")) { accuracyType = "IsNominal"; }
				if (value.ToLower().Equals("isaccurate")) { accuracyType = "IsAccurate"; }
			}
		}

		public string GetName() { return "#Data type"; }

		public override string ToString() {
			var str = new StringBuilder();
			str.Append("Data type: ").AppendLine(DataType)
				.Append("Ion mode: ").AppendLine(IonMode)
				.Append("Accuracy type: ").AppendLine(AccuracyType);
			return str.ToString();
		}
	}

	[JsonObject]
	public class DataCollection : IDataCollection {
		private double retentionTimeBegin = 0;
		private double retentionTimeEnd = 15;
		private double massRangeBegin = 0;
		private double massRangeEnd = 2000;

		[JsonProperty(PropertyName = "rtbegin")]
		public double RetentionTimeBegin {
			get { return retentionTimeBegin; }

			set {
				if (value < 0.0) { value = 0.0; }
				retentionTimeBegin = value;
			}
		}
		[JsonProperty(PropertyName = "rtend")]
		public double RetentionTimeEnd {
			get { return retentionTimeEnd; }

			set {
				if (value < 0.0) { value = 0.0; }
				if(value < retentionTimeBegin) { value = retentionTimeBegin; }
				retentionTimeEnd = value;
			}
		}
		[JsonProperty(PropertyName = "mzrangebegin")]
		public double MassRangeBegin {
			get { return massRangeBegin; }

			set {
				if(value < 0) { value = 0.0; }
				massRangeBegin = value;
			}
		}
		[JsonProperty(PropertyName = "mzrangeend")]
		public double MassRangeEnd {
			get { return massRangeEnd; }

			set {
				if(value < 0) { value = 0.0; }
				if(value < massRangeBegin) { value = massRangeBegin; }
				massRangeEnd = value;
			}
		}

		public string GetName() { return "#Data collection parameters"; }

		public override string ToString() {
			var str = new StringBuilder();
			str.Append("Retention time begin: ").AppendLine(RetentionTimeBegin.ToString())
				.Append("Retention time end: ").AppendLine(RetentionTimeEnd.ToString())
				.Append("Mass range begin: ").AppendLine(MassRangeBegin.ToString())
				.Append("Mass range end: ").AppendLine(massRangeEnd.ToString());

			return str.ToString().ToString();
		}
	}

	[JsonObject]
	public class PeakDetection : IPeakDetection {
		private List<string> methods = new List<string>() { "SimpleMovingAverage", "LinearWeightedMovingAverage", "SavitzkyGolayFilter", "BinomialFilter" };

		private string smoothingMethod = "LinearWeightedMovingAverage";
		private int smoothingLevel = 2;
		private int averagePeakWidth = 20;
		private int minimumPeakHeight = 2000;
		private double massSliceWidth = 0.5;
		private double massAccuracy = 0.5;

		[JsonProperty(PropertyName = "smoothingmethod")]
		public string SmoothingMethod {
			get { return smoothingMethod; }

			set {
				if(!methods.Contains(value)) {
					Console.WriteLine($"Invalid Smoothing Method: {value}");
					value = "LinearWeightedMovingAverage";
				}
				smoothingMethod = value;
			}
		}
		[JsonProperty(PropertyName = "smoothinglevel")]
		public int SmoothingLevel {
			get { return smoothingLevel; }

			set {
				if (value < 1) { value = 1; }
				smoothingLevel = value;
			}
		}
		[JsonProperty(PropertyName = "avgpeakwidth")]
		public int AveragePeakWidth {
			get { return averagePeakWidth; }

			set {
				if(value < 0) { value = 0; }
				averagePeakWidth = value;
			}
		}
		[JsonProperty(PropertyName = "minpeakwidth")]
		public int MinimumPeakHeight {
			get { return minimumPeakHeight; }

			set {
				if(value < 0) { value = 0; }
				minimumPeakHeight = value;
			}
		}
		[JsonProperty(PropertyName = "mzslicewidth")]
		public double MassSliceWidth {
			get { return massSliceWidth; }

			set {
				if(value < 0.0) { value = 0.0; }
				massSliceWidth = value;
			}
		}
		[JsonProperty(PropertyName = "mzaccuracy")]
		public double MassAccuracy {
			get { return massAccuracy; }

			set {
				if (value < 0) { value = 0.0; }
				massAccuracy = value;
			}
		}

		public string GetName() { return "#Peak detection parameters"; }

		public override string ToString() {
			var str = new StringBuilder();
			str.Append("Smoothing method: ").AppendLine(SmoothingMethod)
				.Append("Smoothing level: ").AppendLine(SmoothingLevel.ToString())
				.Append("Average peak width: ").AppendLine(AveragePeakWidth.ToString())
				.Append("Minimum peak height: ").AppendLine(MinimumPeakHeight.ToString())
				.Append("Mass slice width: ").AppendLine(MassSliceWidth.ToString())
				.Append("Mass accuracy: ").AppendLine(MassAccuracy.ToString());

			return str.ToString();
		}
	}

	[JsonObject]
	public class Ms1Deconvolution : IMs1Deconvolution {
		private double sigmaWindowValue = 0.1;
		private int amplitudeCutOff = 10;

		[JsonProperty]
		public double SigmaWindowValue {
			get { return sigmaWindowValue; }
			set {
				if(value < 0.0) { value = 0.0; }
				sigmaWindowValue = value;
			}
		}
		[JsonProperty]
		public int AmplitudeCutOff {
			get { return amplitudeCutOff; }

			set {
				if (value < 0) { value = 0; }
				amplitudeCutOff = value;
			}
		}

		public string GetName() { return "#MS1Dec parameters"; }

		public override string ToString() {
			var str = new StringBuilder();
			str.Append("Sigma window value: ").AppendLine(SigmaWindowValue.ToString())
				.Append("Amplitude cut off: ").AppendLine(AmplitudeCutOff.ToString());

			return str.ToString();
		}
	}

	[JsonObject]
	public class Identification : IIdentification {
		private string mspFile = ""; // Internal resource or file path
		private string riIndexFile = "";
		private string retentionType = "RT"; // RI or RT
		private string riCompound = "Fames"; // Alkanes or Fames
		private double retentionTimeTolerance = 0.5;
		private int retentionIndexTolerance = 3000;
		private double eiSimilarityLibraryTolerance = 70;
		private int identificationScoreCutOff = 70;

		[JsonProperty(PropertyName = "mspfile", Required = Required.Default)]
		public string MSPFile { get; set; }
		[JsonProperty(PropertyName = "riindexfile", Required = Required.Default)]
		public string RIIndexFile { get; set; }
		[JsonProperty(PropertyName = "retentiontype")]
		public string RetentionType { get; set; }
		[JsonProperty(PropertyName = "ricompound")]
		public string RICompound { get; set; }
		[JsonProperty(PropertyName = "rttolerance")]
		public double RetentionTimeTolerance { get; set; }
		[JsonProperty(PropertyName = "ritolerance")]
		public int RetentionIndexTolerance { get; set; }
		[JsonProperty(PropertyName = "eisimilaritytolerance")]
		public double EISimilarityLibraryTolerance { get; set; }
        [JsonProperty(PropertyName = "idscorecutoff")]
        public int IdentificationScoreCutOff { get; set; }

		public string GetName() { return "#identification"; }

		public override string ToString() {
			var str = new StringBuilder();
			if (MSPFile != null && MSPFile.Length > 0) {
				str.Append("MSP file: ").AppendLine(MSPFile);
			}
			if (RIIndexFile != null && RIIndexFile.Length > 0) {
				str.Append("RI index file: ").AppendLine(RIIndexFile);
			}
			str.Append("Retention type: ").AppendLine(RetentionType)
				.Append("RI compound: ").AppendLine(RICompound)
				.Append("Retention time tolerance for identification: ").AppendLine(RetentionTimeTolerance.ToString())
				.Append("Retention index tolerance for identification: ").AppendLine(RetentionIndexTolerance.ToString())
				.Append("EI similarity library tolerance for identification: ").AppendLine(EISimilarityLibraryTolerance.ToString())
				.Append("Identification score cut off: ").AppendLine(IdentificationScoreCutOff.ToString());

			return str.ToString();
		}
	}

	[JsonObject]
	public class Alignment : IAlignment {
		private double retentionTimeTolerance = 0.075;
		private double eiSimilarityTolerance = 70;
		private double retentionTimeFactor = 0.5;
		private double eiSimilarityFactor = 0.5;
		private int peakCountFilter = 0;
		private bool qcAtLeastFilter = true;

		[JsonProperty(PropertyName = "rttolerance")]
		public double RetentionTimeTolerance { get; set; }
		[JsonProperty(PropertyName = "eisimilaritytolerance")]
		public double EISimilarityTolerance { get; set; }
		[JsonProperty(PropertyName = "rtfactor")]
		public double RetentionTimeFactor { get; set; }
		[JsonProperty(PropertyName = "eisimilarityfactor")]
		public double EISimilarityFactor { get; set; }
		[JsonProperty(PropertyName = "peakcountfilter")]
		public int PeakCountFilter { get; set; }
		[JsonProperty(PropertyName = "qcatleastfilter")]
		public bool QCAtLeastFilter { get; set; }

		public string GetName() { return "#Alignment parameters setting"; }

		public override string ToString() {
			var str = new StringBuilder();
			str.Append("Retention time tolerance for alignment: ").AppendLine(RetentionTimeTolerance.ToString())
				.Append("EI similarity tolerance for alignment: ").AppendLine(EISimilarityTolerance.ToString())
				.Append("Retention time factor for alignment: ").AppendLine(RetentionTimeFactor.ToString())
				.Append("EI similarity factor for alignment: ").AppendLine(EISimilarityFactor.ToString())
				.Append("Peak count filter: ").AppendLine(PeakCountFilter.ToString())
				.Append("QC at least filter: ").AppendLine(QCAtLeastFilter?"True":"False");

			return str.ToString();
		}
	}
}
