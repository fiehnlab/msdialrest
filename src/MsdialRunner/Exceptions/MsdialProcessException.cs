﻿using System;

namespace MsdialRunner.Exceptions
{
    public class MsdialProcessException : Exception {
        public long MsdialCode { get; set; }  = 0;
        public string MsdialOutput { get; set; } = "";
        public string MsdialError { get; set; } = "";

        public MsdialProcessException() : base() { }
        public MsdialProcessException(string msg) : base(msg) { }
        public MsdialProcessException(string msg, Exception innerEx) : base(msg, innerEx) { }
    }
}
