﻿using System.IO;
using System.Threading.Tasks;

namespace FServerClient.Api {
    public interface IFServerClient {
        bool Exists(string file);
        Task<bool> Upload(FileInfo file);
        Task<FileInfo> Download(string file, string targetFolder);
        string GetPath(string file);
        string GetAddress();
    }
}
