﻿using FServerClient.Api;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace FServerClient.Model {
    public class FServerCli : IFServerClient {
        private readonly HttpClient client;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="serverAddress">Full URI of running FServer (including port)</param>
        public FServerCli(Uri serverAddress) {
            client = new HttpClient {
                BaseAddress = serverAddress
            };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        /// <summary>
        /// Checks the existence of a file on the File Server
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public bool Exists(string file) {
            using (HttpResponseMessage response = client.GetAsync("rest/file/exists/" + file, HttpCompletionOption.ResponseHeadersRead).Result) {
                var jsonstr = response.Content.ReadAsStringAsync().Result;

                var data = JsonConvert.DeserializeObject<FServerResponse>(jsonstr);
                return data.Exist;
            }
        }

        /// <summary>
        /// Uploads a file to the File Server
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public async Task<bool> Upload(FileInfo file) {
            var content = new MultipartFormDataContent();
            var streamcontent = new StreamContent(file.OpenRead());

            streamcontent.Headers.Add("Content-Disposition", "form-data; name=\"file\"; filename=\"" + file.Name + "\"");
            content.Add(streamcontent, "file", file.Name);

            using (var message = await client.PostAsync("rest/file/upload", content)) {
                try {
                    message.EnsureSuccessStatusCode();
                } catch(Exception ex) {
                    Debug.WriteLine("------ FServCli Error: " + ex.Message);
                    throw ex;
                }
                return message.IsSuccessStatusCode;
            }
        }

        /// <summary>
        /// Donloads a file from the File Server
        /// </summary>
        /// <param name="file"></param>
        /// <param name="targetFolder"></param>
        /// <returns></returns>
        public async Task<FileInfo> Download(string file, string targetFolder) {
            var outFile = Path.Combine(targetFolder, file);
            client.DefaultRequestHeaders.Add("Accept", "application/octet-stream");

            using (HttpResponseMessage response = await client.GetAsync("rest/file/download/" + file, HttpCompletionOption.ResponseHeadersRead))
            using (Stream ourStream = File.Open(outFile, FileMode.Create)) {

                await response.Content.CopyToAsync(ourStream);
            }

            return new FileInfo(outFile);
        }

        /// <summary>
        /// finds the 
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public string GetPath(string file) {
            using (HttpResponseMessage response = client.GetAsync("rest/file/exists/" + file, HttpCompletionOption.ResponseHeadersRead).Result) {
                if (response.IsSuccessStatusCode) {
                    var data = JsonConvert.DeserializeObject<FServerResponse>(response.Content.ReadAsStringAsync().Result);
                    return data.File;
                } else {
                    return $"{file} not found";
                }
            }
        }

        public string GetAddress() => client.BaseAddress.ToString();
    }
}
