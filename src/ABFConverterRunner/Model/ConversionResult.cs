﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ABFConverterRunner.Model {
    /// <summary>
    /// 
    /// </summary>
    public class ConversionResult {
        public int Code { get; set; }
        public string Input { get; set; }
        public string Destination { get; set; }
        public string Error { get; set; }
        public string Output { get; set; }
        public string ConversionStatus { get; set; }
        public string ConversionMessage { get; set; }
        public string RawType { get; set; }
        public string ConversionErrorSeverity { get; set; }
    }
}
