﻿using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using MsdialRunner.Model;
using ABFConverterRunner.Exceptions;
using System.Reflection;
using ABFConverterRunner.Model;
using System.Linq;

namespace ABFConverterRunner {
    public class AbfConverterRunner {
        private static ILogger logger;

        /// <summary>
        /// Runs a conversion creating an abf file
        /// </summary>
        /// <param name="converter">Full file name (including folder) of the console abf .exe</param>
        /// <param name="file">raw file to be converted</param>
        /// <param name="output">folder to save converted data</param>
        /// <param name="log">a logger factory to create a logger</param>
        /// <returns></returns>
        public static ConversionResult Run(string converter, string file, string output, ILoggerFactory log) {
            logger = log.CreateLogger<AbfConverterRunner>();

            if(RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) {
                file = file.Replace("/", "\\");
            }

            if(!new FileInfo(converter).Exists) {
                throw new ConversionException($"Invallid ABFConverter executable '{converter}'");
            }

            try {
                Directory.CreateDirectory(output);
            } catch (Exception ex) {
                logger.LogError($"Cannot create directory: {output}");
                return new ConversionResult() { Code = -2, Error = $"Cannot create directory: {output}\nPrevious: {ex.Message}" };
            }

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = false;
            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardError = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.FileName = converter;
            startInfo.WorkingDirectory = converter.Substring(0, converter.LastIndexOf("\\"));
            startInfo.Arguments = $"-i {file} -o {output}";

            var stdout = "";
            var stderr = "";
            long code = -1;
            try {
                Process conversion = new Process();

                conversion = Process.Start(startInfo);
                stdout = conversion.StandardOutput.ReadToEnd();
                stderr = conversion.StandardError.ReadToEnd();
                conversion.WaitForExit();

                var dest = "";
                if(conversion.ExitCode == 0) {
                    dest = stdout.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).Where(item => item.Contains("Destination: ")).SingleOrDefault().Split(new string[] { ": " }, StringSplitOptions.None)[1];
                }
                var response = new ConversionResult() {Code = conversion.ExitCode, Destination = dest, Output = stdout, Error = stderr };
                code = conversion.ExitCode;
                conversion.Dispose();

                return response;
            } catch(Exception ex) {
                FileInfo fi = new FileInfo(file);
                string msg = $"Error: {ex.Message}\nStack: {ex.StackTrace}\nProcess out: {stdout}\nProcess err: {stderr}";

                logger.LogError(new EventId(), ex, msg);
                if (ex.GetType().IsAssignableFrom(typeof(ConversionException))) {
                    throw (ex);
                } else { 
                    throw new ConversionException(ex.Message, ex) { ConverterCode = code, ConverterOutput = stdout, ConverterError = stderr };
                }
            }
        }
    }
}
