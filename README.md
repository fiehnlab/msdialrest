# MsdialRest #

This is a .Net Core 1.1 service that provides a REST API to do data processing via MSDial.

### How do I get set up? ###

* Install Visual Studio ([2015, 2017 or Code][1])
* Install .Net Core 1.1 or above ([link][2])
* Clone this repository:
```
git clone http://www.bitbucket.org/fiehnlab/msdialrest.git
```
* Go to the **MsdialRest** folder and run the following commands:
```bash
cd msdialrest\src\msdialrest
dotnet restore
dotnet build
dotnet publish -c Release -o app [-r win7-x64] (the optional -r flag is to create an executable file)
```
* Go to the **app** folder and run MsdialRest.exe
* Alternatively you can run the application with
```bash
dotnet run
```

[1]: https://www.visualstudio.com/downloads/
[2]: https://github.com/dotnet/core/blob/master/release-notes/download-archive.md

### How to process data ###

The process is as follows:

Use the form to upload abf files, write a client of your own, or use curl:

```bash
curl -F files=@fileName.abf http://<serverUrl>/rest/upload
```

adding as many `-F files=@...` parameters as files that need to be uploaded; the 'files' name is required since it is what the server variable that holds the file data.

The response is a map (abf file name, schedule url) that can be used to schedule each particular file.

```json
  {
    "filename":"blah.abf", 
    "schedule":"http://server/rest/deconvolution/schedule/<id>"
  } 
```
From this point on all the endpoints need an authorization token to be sent with the request as an Authorization header. This token is the main file to be deconvoluted, e.a.:
```
for a zipped Agilent file (test.d.zip) the authorization token will be test.d
for an abf file the autorization token will be the same filename
```

After the files are on the server calling the schedule url will start the deconvolution process (with default paramerters):

```bash
curl http://server/rest/deconvolution/schedule/<id>
```

To use custom parameters it is necessary to do a POST request to the same url but with a JSON body containing a Parameters object based on the following schema:

[msdial-schema.json][4]

[4]: https://bitbucket.org/fiehnlab/msdialrest/src/d7b92ed32e79b0b581bf952d3feb8f19839652db/src/MsdialRest/wwwroot/Schemas/msdial-schema.json?at=master

The response  is a simple json obj:

```json
{
  "message": "blah.abf was scheduled using parameters from <params file>", 
  "status":"http://server/rest/deconvolution/status/<id>"
}
```

This is a blocking process right now but we can async it later; so the "status" endpoint is useless (the process already finished when you see the response).

Using the url in 'status' returns information about a file:

```bash
curl http://server/rest/deconvolution/status/<id>
```

The response looks like:

```json
{
  "filename":"TestFile.txt",
  "status":"ready",
  "path":"http://server/rest/deconvolution/result/<id>"
}
```

To download the results use the url on 'path'


### Who do I talk to? ###

* [Diego Pedrosa][3] (Lead developer)

[3]:mailto:dpedrosa@ucdavis.edu